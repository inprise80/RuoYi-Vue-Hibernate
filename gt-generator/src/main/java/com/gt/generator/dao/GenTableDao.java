package com.gt.generator.dao;

import com.gt.common.core.dao.BaseDao;
import com.gt.common.core.dao.dialect.FormatUtil;
import com.gt.common.core.page.Page;
import com.gt.common.core.page.PageDomain;
import com.gt.common.core.page.TableSupport;
import com.gt.common.utils.ConvertUtil;
import com.gt.common.utils.ValidateUtil;
import com.gt.generator.domain.GenTable;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 业务 数据层
 *
 * @author liuyj
 */
@Repository
public class GenTableDao extends BaseDao<GenTable,Long> {
    /**
     * 查询业务列表
     *
     * @param genTable 业务信息
     * @return 业务集合
     */
    public Page findGenTableList(GenTable genTable){
        PageDomain pageDomain = TableSupport.buildPageRequest();
        String hql="from GenTable where 1=1 ";
        if (!ValidateUtil.isEmpty(genTable.getTableName())) {
            hql += " and tableName like '%" + genTable.getTableName() + "%'";
        }
        if (!ValidateUtil.isEmpty(genTable.getTableComment())) {
            hql += " and tableComment like '%" + genTable.getTableComment() + "%'";
        }
        if (!ValidateUtil.isEmpty((String) genTable.getParams().get("beginTime"))) {
            hql += " and " + sqlUtil.string2ShortDate(sqlUtil.date2String("createTime"));
            hql += " >= " + sqlUtil.string2ShortDate(FormatUtil.formatStrForDB((String) genTable.getParams().get("beginTime")));
        }
        if (!ValidateUtil.isEmpty((String) genTable.getParams().get("endTime"))) {
            hql += " and " + sqlUtil.string2ShortDate(sqlUtil.date2String("createTime"));
            hql += " <= " + sqlUtil.string2ShortDate(FormatUtil.formatStrForDB((String) genTable.getParams().get("endTime")));
        }
        return this.findPage(hql, pageDomain.getPageNum(), pageDomain.getPageSize().intValue());
    }

    /**
     * 查询据库列表
     *
     * @param genTable 业务信息
     * @return 数据库表集合
     */
    public Page findDbTableList(GenTable genTable){
        PageDomain pageDomain = TableSupport.buildPageRequest();
        StringBuilder sql = new StringBuilder();
        sql.append(" select table_name as tableName, table_comment as tableComment, create_time as createTime, update_time as updateTime");
        sql.append(" from information_schema.tables");
        sql.append("  where table_schema = (select database())  ");
        sql.append("  AND table_name NOT LIKE 'qrtz_%' AND table_name NOT LIKE 'gen_%'  ");
        sql.append("  AND table_name NOT IN (select table_name from gen_table)  ");
        if (!ValidateUtil.isEmpty(genTable.getTableName())) {
            sql.append(" AND lower(table_name) like '%");
            sql.append(genTable.getTableName().toLowerCase()+"%'");
        }
        if (!ValidateUtil.isEmpty(genTable.getTableComment())) {
            sql.append(" AND lower(table_comment) like '%");
            sql.append(genTable.getTableComment().toLowerCase()+"%'");
        }
        if (!ValidateUtil.isEmpty((String) genTable.getParams().get("beginTime"))) {
            sql.append(" and " + sqlUtil.string2ShortDate(sqlUtil.date2String("createTime")));
            sql.append(" >= " + sqlUtil.string2ShortDate(FormatUtil.formatStrForDB((String) genTable.getParams().get("beginTime"))));
        }
        if (!ValidateUtil.isEmpty((String) genTable.getParams().get("endTime"))) {
            sql.append(" and " + sqlUtil.string2ShortDate(sqlUtil.date2String("createTime")));
            sql.append(" <= " + sqlUtil.string2ShortDate(FormatUtil.formatStrForDB((String) genTable.getParams().get("endTime"))));
        }
        sql.append(" order by create_time desc");
        return this.findPageByFreeSQL(sql.toString(), pageDomain.getPageNum(), pageDomain.getPageSize().intValue(),GenTable.class);
    }

    /**
     * 查询据库列表
     *
     * @param tableNames 表名称组
     * @return 数据库表集合
     */
    public List<GenTable> findDbTableListByNames(String[] tableNames){
        StringBuilder sql = new StringBuilder();
        sql.append(" select table_name as tableName, table_comment as tableComment, create_time as createTime, update_time as updateTime ");
        sql.append(" from information_schema.tables");
        sql.append(" where table_name NOT LIKE 'qrtz_%' and table_name NOT LIKE 'gen_%' and table_schema = (select database()) ");
        if (!ValidateUtil.isEmpty(tableNames)) {
            sql.append(" and table_name in ");
            sql.append(ConvertUtil.toDbString(tableNames));
        }
        return this.findBySQL(sql.toString(),GenTable.class);
    }

    /**
     * 查询所有表信息
     *
     * @return 表信息集合
     */
    public List<GenTable> findGenTableAll(){
        return null;
    }


    /**
     * 查询表名称业务信息
     *
     * @param tableName 表名称
     * @return 业务信息
     */
    public GenTable selectGenTableByName(String tableName){
        GenTable genTable=null;
        String hql="from GenTable where tableName=?1";
        List<GenTable> list=this.find(hql,tableName);
        if(!ValidateUtil.isEmpty(list)){
            genTable=list.get(0);
        }
        return genTable;
    }
}
