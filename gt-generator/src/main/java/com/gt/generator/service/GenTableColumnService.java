package com.gt.generator.service;

import java.util.List;

import com.gt.common.core.dao.BaseDao;
import com.gt.common.core.service.BaseService;
import com.gt.generator.dao.GenTableColumnDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.gt.common.core.text.Convert;
import com.gt.generator.domain.GenTableColumn;

/**
 * 业务字段 服务层实现
 * 
 * @author liuyj
 */
@Service
public class GenTableColumnService extends BaseService<GenTableColumn,Long>
{
	@Autowired
	private GenTableColumnDao genTableColumnDao;
	@Override
	protected BaseDao<GenTableColumn, Long> getDao() {
		return null;
	}

	/**
     * 查询业务字段列表
     * 
     * @param tableId 业务字段编号
     * @return 业务字段集合
     */
	public List<GenTableColumn> selectGenTableColumnListByTableId(Long tableId)
	{
	    return genTableColumnDao.findByTableId(tableId);
	}
	
    /**
     * 新增业务字段
     * 
     * @param genTableColumn 业务字段信息
     * @return 结果
     */
	public void insertGenTableColumn(GenTableColumn genTableColumn)
	{
	    genTableColumnDao.addObject(genTableColumn);
	}
	
	/**
     * 修改业务字段
     * 
     * @param genTableColumn 业务字段信息
     * @return 结果
     */
	public void updateGenTableColumn(GenTableColumn genTableColumn)
	{
		genTableColumnDao.updateObject(genTableColumn);
	}

	/**
     * 删除业务字段对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteGenTableColumnByIds(String ids)
	{
		return genTableColumnDao.deleteGenTableColumnByIds(Convert.toLongArray(ids));
	}

}
