package com.gt.quartz.dao;

import com.gt.common.core.dao.BaseDao;
import com.gt.common.core.page.Page;
import com.gt.common.core.page.PageDomain;
import com.gt.common.core.page.TableSupport;
import com.gt.common.utils.ValidateUtil;
import com.gt.quartz.domain.SysJob;
import org.springframework.stereotype.Repository;

/**
 * 调度任务信息 数据层
 *
 * @author liuyj
 */
@Repository
public class SysJobDao extends BaseDao<SysJob,Long> {
    /**
     * 查询调度任务日志集合(分页)
     *
     * @param job 调度信息
     * @return 操作日志集合
     */
    public Page selectJobList(SysJob job){
        PageDomain pageDomain = TableSupport.buildPageRequest();
        String hql="from SysJob where 1=1 ";
        if (!ValidateUtil.isEmpty(job.getJobName())) {
            hql += " and jobName like '%" + job.getJobName() + "%'";
        }
        if (!ValidateUtil.isEmpty(job.getJobGroup())) {
            hql += " and jobGroup= '" + job.getJobGroup() + "'";
        }
        if (!ValidateUtil.isEmpty(job.getStatus())) {
            hql += " and status= '" + job.getStatus() + "'";
        }
        if (!ValidateUtil.isEmpty(job.getInvokeTarget())) {
            hql += " and invokeTarget like '%" + job.getInvokeTarget() + "%'";
        }
        return this.findPage(hql, pageDomain.getPageNum(), pageDomain.getPageSize().intValue());
    }

}
