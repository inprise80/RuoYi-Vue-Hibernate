package com.gt.quartz.dao;

import com.gt.common.core.dao.BaseDao;
import com.gt.common.core.dao.dialect.FormatUtil;
import com.gt.common.core.page.Page;
import com.gt.common.core.page.PageDomain;
import com.gt.common.core.page.TableSupport;
import com.gt.common.utils.ValidateUtil;
import com.gt.quartz.domain.SysJobLog;
import org.springframework.stereotype.Repository;

/**
 * 调度任务日志信息 数据层
 *
 * @author liuyj
 */
@Repository
public class SysJobLogDao extends BaseDao<SysJobLog,Long> {
    /**
     * 获取quartz调度器日志的计划任务(分页)
     *
     * @param jobLog 调度日志信息
     * @return 调度任务日志集合
     */
    public Page selectJobLogList(SysJobLog jobLog){
        PageDomain pageDomain = TableSupport.buildPageRequest();
        String hql="from SysJobLog where 1=1 ";
        if (!ValidateUtil.isEmpty(jobLog.getJobName())) {
            hql += " and jobName like '%" + jobLog.getJobName() + "%'";
        }
        if (!ValidateUtil.isEmpty(jobLog.getJobGroup())) {
            hql += " and jobGroup= '" + jobLog.getJobGroup() + "'";
        }
        if (!ValidateUtil.isEmpty(jobLog.getStatus())) {
            hql += " and status= '" + jobLog.getStatus() + "'";
        }
        if (!ValidateUtil.isEmpty(jobLog.getInvokeTarget())) {
            hql += " and invokeTarget like '%" + jobLog.getInvokeTarget() + "%'";
        }
        if (!ValidateUtil.isEmpty((String) jobLog.getParams().get("beginTime"))) {
            hql += " and " + sqlUtil.string2ShortDate(sqlUtil.date2String("createTime"));
            hql += " >= " + sqlUtil.string2ShortDate(FormatUtil.formatStrForDB((String) jobLog.getParams().get("beginTime")));
        }
        if (!ValidateUtil.isEmpty((String) jobLog.getParams().get("endTime"))) {
            hql += " and " + sqlUtil.string2ShortDate(sqlUtil.date2String("createTime"));
            hql += " <= " + sqlUtil.string2ShortDate(FormatUtil.formatStrForDB((String) jobLog.getParams().get("endTime")));
        }
        return this.findPage(hql, pageDomain.getPageNum(), pageDomain.getPageSize().intValue());
    }


    /**
     * 清空任务日志
     */
    public void cleanJobLog(){
        String sql="truncate table sys_job_log";
        this.executeSql(sql);
    }
}
