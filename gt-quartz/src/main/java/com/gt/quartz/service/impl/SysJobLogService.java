package com.gt.quartz.service.impl;

import com.gt.common.core.dao.BaseDao;
import com.gt.common.core.page.Page;
import com.gt.common.core.service.BaseService;
import com.gt.quartz.dao.SysJobLogDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.gt.quartz.domain.SysJobLog;

/**
 * 定时任务调度日志信息 服务层
 * 
 * @author liuyj
 */
@Service
public class SysJobLogService extends BaseService<SysJobLog,Long>
{
    @Autowired
    private SysJobLogDao sysJobLogDao;
    @Override
    protected BaseDao<SysJobLog, Long> getDao() {
        return sysJobLogDao;
    }

    /**
     * 获取quartz调度器日志的计划任务
     * 
     * @param jobLog 调度日志信息
     * @return 调度任务日志集合
     */
    public Page selectJobLogList(SysJobLog jobLog)
    {
        return sysJobLogDao.selectJobLogList(jobLog);
    }

    /**
     * 清空任务日志
     */
    public void cleanJobLog()
    {
        sysJobLogDao.cleanJobLog();
    }

}
