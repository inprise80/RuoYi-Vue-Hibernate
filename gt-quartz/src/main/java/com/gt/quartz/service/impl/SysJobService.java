package com.gt.quartz.service.impl;

import java.util.List;
import javax.annotation.PostConstruct;

import com.gt.common.core.dao.BaseDao;
import com.gt.common.core.page.Page;
import com.gt.common.core.service.BaseService;
import com.gt.quartz.dao.SysJobDao;
import org.quartz.JobDataMap;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.gt.common.constant.ScheduleConstants;
import com.gt.common.exception.job.TaskException;
import com.gt.quartz.domain.SysJob;
import com.gt.quartz.util.CronUtils;
import com.gt.quartz.util.ScheduleUtils;

/**
 * 定时任务调度信息 服务层
 * 
 * @author liuyj
 */
@Service
public class SysJobService extends BaseService<SysJob,Long>
{
    @Autowired
    private Scheduler scheduler;

    @Autowired
    private SysJobDao sysJobDao;
    @Override
    protected BaseDao<SysJob, Long> getDao() {
        return sysJobDao;
    }

    /**
     * 项目启动时，初始化定时器 主要是防止手动修改数据库导致未同步到定时任务处理（注：不能手动修改数据库ID和任务组名，否则会导致脏数据）
     */
    @PostConstruct
    public void init() throws SchedulerException, TaskException
    {
        scheduler.clear();
        List<SysJob> jobList = sysJobDao.getObjects();
        for (SysJob job : jobList)
        {
            ScheduleUtils.createScheduleJob(scheduler, job);
        }
    }

    /**
     * 获取quartz调度器的计划任务列表(分页)
     * 
     * @param job 调度信息
     * @return
     */
    public Page selectJobList(SysJob job)
    {
        return sysJobDao.selectJobList(job);
    }



    /**
     * 暂停任务
     * 
     * @param job 调度信息
     */
    @Transactional(rollbackFor = Exception.class)
    public void pauseJob(SysJob job) throws SchedulerException
    {
        Long jobId = job.getJobId();
        String jobGroup = job.getJobGroup();
        job.setStatus(ScheduleConstants.Status.PAUSE.getValue());
        sysJobDao.updateObject(job);
        scheduler.pauseJob(ScheduleUtils.getJobKey(jobId, jobGroup));
    }

    /**
     * 恢复任务
     * 
     * @param job 调度信息
     */
    @Transactional(rollbackFor = Exception.class)
    public void resumeJob(SysJob job) throws SchedulerException
    {
        Long jobId = job.getJobId();
        String jobGroup = job.getJobGroup();
        job.setStatus(ScheduleConstants.Status.NORMAL.getValue());
        sysJobDao.updateObject(job);
        scheduler.resumeJob(ScheduleUtils.getJobKey(jobId, jobGroup));
    }

    /**
     * 删除任务后，所对应的trigger也将被删除
     * 
     * @param job 调度信息
     */
    @Transactional(rollbackFor = Exception.class)
    public void deleteJob(SysJob job) throws SchedulerException
    {
        Long jobId = job.getJobId();
        String jobGroup = job.getJobGroup();
        sysJobDao.deleteObject(jobId);
        scheduler.deleteJob(ScheduleUtils.getJobKey(jobId, jobGroup));
    }

    /**
     * 批量删除调度信息
     * 
     * @param jobIds 需要删除的任务ID
     * @return 结果
     */
    @Transactional(rollbackFor = Exception.class)
    public void deleteJobByIds(Long[] jobIds) throws SchedulerException
    {
        for (Long jobId : jobIds)
        {
            deleteObject(jobId);
        }
    }

    /**
     * 任务调度状态修改
     * 
     * @param job 调度信息
     */
    @Transactional(rollbackFor = Exception.class)
    public void changeStatus(SysJob job) throws SchedulerException
    {
        int rows = 0;
        String status = job.getStatus();
        if (ScheduleConstants.Status.NORMAL.getValue().equals(status))
        {
            resumeJob(job);
        }
        else if (ScheduleConstants.Status.PAUSE.getValue().equals(status))
        {
            pauseJob(job);
        }
    }

    /**
     * 立即运行任务
     * 
     * @param job 调度信息
     */
    @Transactional(rollbackFor = Exception.class)
    public void run(SysJob job) throws SchedulerException
    {
        Long jobId = job.getJobId();
        String jobGroup = job.getJobGroup();
        SysJob properties = (SysJob) getObject(job.getJobId());
        // 参数
        JobDataMap dataMap = new JobDataMap();
        dataMap.put(ScheduleConstants.TASK_PROPERTIES, properties);
        scheduler.triggerJob(ScheduleUtils.getJobKey(jobId, jobGroup), dataMap);
    }

    /**
     * 新增任务
     * 
     * @param job 调度信息 调度信息
     */
    @Transactional(rollbackFor = Exception.class)
    public void insertJob(SysJob job) throws SchedulerException, TaskException
    {
        job.setStatus(ScheduleConstants.Status.PAUSE.getValue());
        sysJobDao.addObject(job);
        ScheduleUtils.createScheduleJob(scheduler, job);
    }

    /**
     * 更新任务的时间表达式
     * 
     * @param job 调度信息
     */
    @Transactional(rollbackFor = Exception.class)
    public void updateJob(SysJob job) throws SchedulerException, TaskException
    {
        SysJob properties =(SysJob) getObject(job.getJobId());
        sysJobDao.updateObject(job);
        updateSchedulerJob(job, properties.getJobGroup());
    }

    /**
     * 更新任务
     * 
     * @param job 任务对象
     * @param jobGroup 任务组名
     */
    public void updateSchedulerJob(SysJob job, String jobGroup) throws SchedulerException, TaskException
    {
        Long jobId = job.getJobId();
        // 判断是否存在
        JobKey jobKey = ScheduleUtils.getJobKey(jobId, jobGroup);
        if (scheduler.checkExists(jobKey))
        {
            // 防止创建时存在数据问题 先移除，然后在执行创建操作
            scheduler.deleteJob(jobKey);
        }
        ScheduleUtils.createScheduleJob(scheduler, job);
    }

    /**
     * 校验cron表达式是否有效
     * 
     * @param cronExpression 表达式
     * @return 结果
     */
    public boolean checkCronExpressionIsValid(String cronExpression)
    {
        return CronUtils.isValid(cronExpression);
    }
}
