package com.gt.common.core.dao.dialect;
import org.hibernate.SessionFactory;

/**
 * MySql特有函数适配类
 * @author liuyj
 */
public class MySqlStyleFunction extends DialectAdapter{
	public MySqlStyleFunction() {
	}

	public MySqlStyleFunction(SessionFactory session) {
		super(session); 
	}
	
	protected void initAdapter() {
		dialect = new MySqlDialectEx();
	}
	
	public int getCurrentDBType() {
		return MYSQL;
	}

	public String string2Num(String str) { 
		return "convert("+str+",DECIMAL)";
	}

	public String string2Date(String str) {   
		
		
		return "str_to_date("+str+",'%Y-%m-%d %T')";
	}

	public String string2ShortDate(String str) { 
		return "str_to_date("+str+",'%Y-%m-%d')";
	} 
	public String string2Date(String str, String format) { 
		format= format.replace("yyyy","%Y")
		.replace("MM","%m")
		.replace("dd","%d")
		.replace("HH","%H")
		.replace("mm","%i")
		.replace("ss","%S");
		return "str_to_date("+str+","+FormatUtil.formatStrForDB(format)+")";
	}

	public String date2String(String field) { 
		return "date_format("+field+",'%Y-%m-%d %T')"; 
	}

	public String date2String(String field, String format) {
		format= format.replace("yyyy","%Y")
		.replace("MM","%m")
		.replace("dd","%d")
		.replace("HH","%H")
		.replace("mm","%i")
		.replace("ss","%S");
		return "date_format("+field+","+FormatUtil.formatStrForDB(format)+")"; 
	}

	public String getUUID() { 
		return "replace(uuid(),'-','') ";
	}
	
	public String isNull(String str) {
		return "(" + str + " is null or " + str + "='')";
	}

	public String isNotNull(String str) {
		return "(" + str + " is not null or " + str + "<>'')";
	}
	
	public String replaceNull(String field, String val) {
		return "IFNULL(" + field + "," + val + ")";
	}
	

}
