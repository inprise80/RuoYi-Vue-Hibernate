package com.gt.common.core.page;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 分页类
 * @author liuyj
 *  实现列表的分页封装，包括返回集合、总数量等
 */
public class Page implements Serializable {
    private int totalCount = 0;
    private List items = new ArrayList();

    public Page(List items, int totalCount) {
        this.totalCount = totalCount;
        this.items = items;
    }

    /**
     * 获取查询结果集合
     * @return 查询结果集合
     */
    public List getItems() {
        return items;
    }

    /**
     * 获取数据总数
     * @return
     */
    public int getTotalCount() {
        return totalCount;
    }

}

