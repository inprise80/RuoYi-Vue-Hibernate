package com.gt.common.core.dao.dialect;

import org.hibernate.dialect.Oracle10gDialect;
import org.hibernate.dialect.function.StandardSQLFunction;
import org.hibernate.type.DoubleType;
import org.hibernate.type.StandardBasicTypes;

/**
 * ORACLE数据库函数扩展类
 * @author liuyj
 *         扩展函数列表：</br> to_number：字符转数字（double)</br>
 */
public class OracleDialectEx extends Oracle10gDialect {
	protected void registerFunctions() {
		super.registerFunctions(); 
		registerFunction("to_number", new StandardSQLFunction("to_number", StandardBasicTypes.DOUBLE));
		//registerFunction("to_number", new StandardSQLFunction("to_number", DoubleType.INSTANCE));
	}
}
