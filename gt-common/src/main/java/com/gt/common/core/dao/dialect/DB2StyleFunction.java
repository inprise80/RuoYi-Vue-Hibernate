package com.gt.common.core.dao.dialect;

import java.util.ArrayList;
import java.util.List;


import org.hibernate.SessionFactory;
import org.hibernate.type.StandardBasicTypes;

/**
 * DB2特有函数适配类
 * @author liuyj
 */
public class DB2StyleFunction extends DialectAdapter {
	public DB2StyleFunction() {
	}

	public DB2StyleFunction(SessionFactory session) {
		super(session);
	}

	protected void initAdapter() {
		dialect = new DB2DialectEx();
	}

	public int getCurrentDBType() {
		return IBM_DB2;
	}

	public String string2Num(String str) {
		List params = new ArrayList();
		params.add(str);
		//2022-2-10 update Hibernate.DOUBLE已弃用，改用StandardBasicTypes.DOUBLE
		//params.add(Hibernate.DOUBLE.getName());
		params.add(StandardBasicTypes.DOUBLE.getName());
		return execute(getFunc("cast"), params);
	}

	public String string2Date(String str) {
		// DB2日期作为查询条件可以不用转换
		if (!isFieldName(str)) return str;
		List params = new ArrayList();
		params.add(str);
		//2022-2-10 update Hibernate.DATE已弃用，改用StandardBasicTypes.DATE
		//params.add(Hibernate.DATE.getName());
		params.add(StandardBasicTypes.DATE.getName());
		return execute(getFunc("cast"), params);
	}

	public String string2ShortDate(String str) {
		// DB2日期作为查询条件可以不用转换
		if (!isFieldName(str)) return str;
		List params = new ArrayList();
		params.add(str);
		//2022-2-10 update Hibernate.DATE已弃用，改用StandardBasicTypes.DATE
		//params.add(Hibernate.DATE.getName());
		params.add(StandardBasicTypes.DATE.getName());
		return execute(getFunc("cast"), params);
	}

	public String string2Date(String str, String format) {
		// DB2日期作为查询条件可以不用转换
		if (!isFieldName(str)) return str;
		List params = new ArrayList();
		params.add(str);
		//2022-2-10 update Hibernate.DATE已弃用，改用StandardBasicTypes.DATE
		//params.add(Hibernate.DATE.getName());
		params.add(StandardBasicTypes.DATE.getName());
		return execute(getFunc("cast"), params);
	}

	public String date2String(String field) {
		// DB2日期作为查询条件可以不用转换
		if (!isFieldName(field)) return field;
		List params = new ArrayList();
		params.add(field);
		//2022-2-10 update Hibernate.DATE已弃用，改用StandardBasicTypes.DATE
		//params.add(Hibernate.DATE.getName());
		params.add(StandardBasicTypes.DATE.getName());
		return execute(getFunc("cast"), params);
	}

	public String date2String(String field, String format) {
		// DB2日期作为查询条件可以不用转换
		if (!isFieldName(field)) return field;
		List params = new ArrayList();
		params.add(field);
		//2022-2-10 update Hibernate.DATE已弃用，改用StandardBasicTypes.DATE
		//params.add(Hibernate.DATE.getName());
		params.add(StandardBasicTypes.DATE.getName());
		return execute(getFunc("cast"), params);
	}

	public String getUUID() {
		return "sys_guid()";
	}
}
