package com.gt.common.core.dao.dialect;

import org.hibernate.dialect.DB2Dialect;

/**
 * DB2数据库函数扩展类
 * @author liuyj
 *         扩展函数列表：</br>
 *         
 */
public class DB2DialectEx extends DB2Dialect {
	public DB2DialectEx() {
		super();
//		registerFunction("abs", new StandardSQLFunction("abs"));
	}
}
