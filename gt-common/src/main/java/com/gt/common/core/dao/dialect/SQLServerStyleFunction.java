package com.gt.common.core.dao.dialect;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.type.StandardBasicTypes;

/**
 * SQL Server特有函数适配类
 * @author liuyj
 */
public class SQLServerStyleFunction extends DialectAdapter {
	public SQLServerStyleFunction() {
	}

	public SQLServerStyleFunction(SessionFactory session) {
		super(session);
	}

	protected void initAdapter() {
		dialect = new SQLServerDialectEx();
	}

	public int getCurrentDBType() {
		return MSSQL;
	}

	public String string2Num(String str) {
		List params = new ArrayList();
		params.add(str);
        //2022-2-10 update Hibernate.DOUBLE已弃用，改用StandardBasicTypes.DOUBLE
        //params.add(Hibernate.DOUBLE.getName());
        params.add(StandardBasicTypes.DOUBLE.getName());
		return execute(getFunc("convert"), params);
	}

	public String string2Date(String str) {
		// Sql server日期作查询条件可以不用转换
		if (!isFieldName(str)) return str;
		List params = new ArrayList();
		params.add(str);
        //2022-2-10 update Hibernate.DATE已弃用，改用StandardBasicTypes.DATE
        //params.add(Hibernate.DATE.getName());
        params.add(StandardBasicTypes.DATE.getName());
		return execute(getFunc("convert"), params);
	}

	public String num2String(String str) {
		List params = new ArrayList();
		params.add(str);
        //2022-2-10 update Hibernate.STRING已弃用，改用StandardBasicTypes.STRING
        //params.add(Hibernate.STRING.getName());
		params.add(StandardBasicTypes.STRING.getName());
		return execute(getFunc("convert"), params);
	}

	public String string2ShortDate(String str) {
		// Sql server日期作查询条件可以不用转换
		if (!isFieldName(str)) return str;
		List params = new ArrayList();
		params.add(str);
        //2022-2-10 update Hibernate.DATE已弃用，改用StandardBasicTypes.DATE
        //params.add(Hibernate.DATE.getName());
        params.add(StandardBasicTypes.DATE.getName());
		return execute(getFunc("convert"), params);
	}

	public String string2Date(String str, String format) {
		// Sql server日期作查询条件可以不用转换
		if (!isFieldName(str)) return str;
		List params = new ArrayList();
		params.add(str);
        //2022-2-10 update Hibernate.DATE已弃用，改用StandardBasicTypes.DATE
        //params.add(Hibernate.DATE.getName());
        params.add(StandardBasicTypes.DATE.getName());
		return execute(getFunc("convert"), params);
	}

	public String date2String(String field) {
		// Sql server日期作查询条件可以不用转换
		if (!isFieldName(field)) return field;
		return "convert(char(10)," + field + ",20)";
	}

	public String date2String(String field, String format) {
		// Sql server日期作查询条件可以不用转换
		if (!isFieldName(field)) return field;
		List params = new ArrayList();
		params.add(field);
        //2022-2-10 update Hibernate.DATE已弃用，改用StandardBasicTypes.DATE
        //params.add(Hibernate.DATE.getName());
        params.add(StandardBasicTypes.DATE.getName());
		return execute(getFunc("convert"), params);
	}

	public String getUUID() {
		return "replace(newid(),'-','')";
	}

	public String isNull(String str) {
		return "(" + str + " is null or " + str + "='')";
	}

	public String isNotNull(String str) {
		return "(" + str + " is not null or " + str + "<>'')";
	}

	public String replaceNull(String field, String val) {
		return "isNull(" + field + "," + val + ")";
	}

}
