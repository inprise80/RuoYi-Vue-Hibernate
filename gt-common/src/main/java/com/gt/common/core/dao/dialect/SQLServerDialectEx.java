package com.gt.common.core.dao.dialect;

import org.hibernate.dialect.SQLServerDialect;

/**
 * 数据库函数扩展类
 * @author liuyj
 *         扩展函数列表：</br>convert：转换函数
 */
public class SQLServerDialectEx extends SQLServerDialect {
	public SQLServerDialectEx() {
		super();
		//registerFunction("convert", new ConvertFunction());
	}

}
