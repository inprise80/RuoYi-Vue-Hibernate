package com.gt.common.core.service;

import com.gt.common.core.dao.BaseDao;
import com.gt.common.core.page.Page;

import java.io.Serializable;
import java.util.List;

/**
 * 基础服务类
 * @author liuyj
 * 实现基础的服务操作功能，包括对象获取、分页获取、新增、批量删除、修改等操作
 */

public abstract class BaseService<T, ID extends Serializable> {
    protected abstract BaseDao<T, ID> getDao();

    /**
     * 获取model对象
     * @param id id
     * @return model对象
     * 
     */
    public  Object getObject(String id)  {
        return getDao().getObject(id);

    }

    /**
     * 获取model对象
     * @param id id
     * @return model对象
     * 
     */
    public  Object getObject(long id)  {        
        return getDao().getObject(id);        
    }

    /**
     * 获取model对象
     * @param id id
     * @return model对象
     * 
     */
    public  Object getObject(int id)  {        
        return getDao().getObject(id);       
    }

    /**
     * 添加model对象
     * @param modelObject model对象
     * 
     */
    public  void addObject(Object modelObject)  {
        getDao().addObject(modelObject);        
    }

    /**
     * 保存model对象
     * @param modelObject model对象
     * 
     */
    public  void saveObject(Object modelObject)  {       
        getDao().saveObject(modelObject);        
    }

    /**
     * 更新model对象
     * @param modelObject model对象
     * 
     */
    public  void updateObject(Object modelObject)  {
        getDao().updateObject(modelObject);        
    }

    /**
     * 删除model对象
     * @param id id
     * 
     */
    public  void deleteObject(String id)  {
        getDao().deleteObject(id);        
    }

    /**
     * 删除model对象
     * @param id id
     *
     */
    public  void deleteObject(Long id)  {
        getDao().deleteObject(id);
    }

    /**
     * 批量删除model对象
     * @param ids id
     * 
     */
    public  void deleteObject(String[] ids)  {        
        for (int i = 0; i < ids.length; i++) {            
            getDao().deleteObject(ids[i]);
        }       
    }

    /**
     * 批量删除model对象
     * @param ids id
     * 
     */
    public  void deleteObject(Long[] ids)  {
        for (int i = 0; i < ids.length; i++) {
            getDao().deleteObject(ids[i]);
        }        
    }

    /**
     * 获取model集合
     * @return model集合
     * 
     */
    //@Transactional
    public  List getObjects()  {        
        return getDao().getObjects();        
    }

    /**
     * 分布查询所有数据
     * @param currPage 当前页
     * @param pageSize 每页显示行数
     * @return 查询结果
     */
    public Page findPage(int currPage, int pageSize)  {
        return getDao().findPage(currPage, pageSize);        
    }

}

