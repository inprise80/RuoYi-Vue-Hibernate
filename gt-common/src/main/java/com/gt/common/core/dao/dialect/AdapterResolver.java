package com.gt.common.core.dao.dialect;

import java.sql.Connection;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 根据数据库连接类型创建不同的函数适配器，通过分析数据库会话属性来判断数据库类型。
 * 经过比较IBM DB2/ORACLE/MSSQLSERVER目前各种版本中的函数库没有存在差异，
 * 因此不再区分版本了，特别编写了一个测试用例来简单判断这些差异。
 */
public class AdapterResolver {
	protected final Logger log = LoggerFactory.getLogger(getClass());

	/**
	 * 返回会话对应的数据库方言函数适配器实例
	 * @param sessionFactory
	 * @return
	 */
	public FuncAdapter resolveAdapter(SessionFactory sessionFactory) {
		Connection con=null;
		FuncAdapter res=new MySqlStyleFunction(sessionFactory);
		Session session = sessionFactory.getCurrentSession();
		if (session != null) {
			res = session.doReturningWork(
					connection -> {
						String databaseName = connection.getMetaData().getDatabaseProductName();
						if (databaseName.startsWith("DB2/")) {
							return new DB2StyleFunction(sessionFactory);
						}
						if ("Oracle".equals(databaseName)) {
							return new OracleStyleFunction(sessionFactory);
						}
						if (databaseName.startsWith("Microsoft SQL Server")) {
							return new SQLServerStyleFunction(sessionFactory);
						}
						if("MySQL".equals(databaseName)){
							return new MySqlStyleFunction(sessionFactory);
						}
						return null;
					}
			);
			//session.close();
		}
		return res;
	}

	/**
	 * 返回会话对应的数据库方言函数适配器实例
	 * @param dbType 数据库类型 db2/oracle/sql_server/mysql
	 * @return
	 */
	public FuncAdapter resolveAdapter(String dbType) {
		switch (dbType.toUpperCase()){
			case "DB2"://DB2数据库
				return new DB2StyleFunction();

			case "ORACLE"://ORACLE数据库
				return new OracleStyleFunction();

			case "SQL_SERVER"://SQL SERVER数据库
				return new SQLServerStyleFunction();

			case "MYSQL"://MYSQL数据库
				return new MySqlStyleFunction();

			default:
				return new MySqlStyleFunction();

		}
	}
}
