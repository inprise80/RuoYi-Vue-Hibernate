package com.gt.common.core.dao.dialect;
import java.util.List;

/**
 * Oralce/DB2/MSSQLServer/MYSQL数据库函数适配抽象接口，主要处理SQL脚本中常用的函数适配问题</br>
 * 由于函数传参时有时是常量，有时是字段名称，因此所有函数的参数都采用字符型参数，返回值也是字符型的。
 * 如果是参数本身就是字符型常量，请使用FormatUtil.formatStrForDB()进行数据格式化，也可以直接用‘号括起来。
 * @author liuyj
 *
 */
public interface FuncAdapter {

    public final static int IBM_DB2 = 0;
    public final static int ORACLE = 1;
    public final static int MSSQL = 2;
    public final static int MYSQL = 3;

    /**
     * 获得当前数据库类型常量
     * @return 当前数据库类型常量（int)
     */
    public int getCurrentDBType();

    /**
     * 类别：字符串处理</br> 字符串连接函数
     * @param params 参数个数不限制
     * @return 对应数据库类型的字符串
     */
    public String concat(List params);

    /**
     * 类别：字符串处理</br> 两个字符串连接函数
     * @param str1
     * @param str2
     * @return
     */
    public String concat(String str1, String str2);

    /**
     * 类别：字符串处理</br> 字符串截取函数
     * @param str 字符串
     * @param start 开始位置，从1开始计数
     * @param count 截取数量
     * @return
     */
    public String substr(String str, int start, int count);

    /**
     * 类别：字符串处理</br> 去除字符串左右两边的空格
     * @param str
     * @return
     */
    public String trim(String str);

    /**
     * 类别：类型转换</br> 数值型数据转字符型
     * @param str
     * @return
     */
    public String num2String(String str);

    /**
     * 类别：类型转换</br> 字符型数据转数值型。内部封装，不暴露给外部，请使用各个数据库风格的函数调用
     * @param str
     * @return
     */
    public String string2Num(String str);

    /**
     * 类别：类型转换</br> 字符型数据转数值型。等同于str2num,to_number,convert函数
     * @param str
     * @return
     */
    public String castString2Num(String str);

    /**
     * 类别：类型转换</br> yyyy-mm-dd hh24:mi:ss字符型数据转日期型。内部封装，不暴露给外部，请使用各个数据库风格的函数调用
     * @param str
     * @return
     */
    public String string2Date(String str);

    /**
     * 短日期格式</br> yyyy-mm-dd
     * @param str
     * @return
     */
    public String string2ShortDate(String str);

    /**
     * 日期格式由外面控制
     * @param str
     * @param format
     * @return
     */
    public String string2Date(String str, String format);

    /**
     * 日期转字符yyyy-mm-dd hh24:mi:ss
     * @param field
     * @return
     */
    public String date2String(String field);

    /**
     * 日期转字符，格式由调用者控制
     * @param field
     * @param format
     * @return
     */
    public String date2String(String field, String format);

    /**
     * 判断一个字段数据是否为null或者''
     * @param str
     * @return
     */
    public String isNull(String str);

    /**
     * 判断一个字段数据是否不为null或者''
     * @param str
     * @return
     */
    public String isNotNull(String str);

    /**
     * 获取数据库主键UUID
     * @return
     */
    public String getUUID();

    /**
     * 如果field的值为空，用val替换，只能用于Select语句
     * @param field
     * @param val
     * @return
     */
    public String replaceNull(String field, String val);

}
