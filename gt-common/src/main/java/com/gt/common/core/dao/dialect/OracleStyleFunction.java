package com.gt.common.core.dao.dialect;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;

/**
 * Oracle特有函数适配类
 * @author liuyj
 */
public class OracleStyleFunction extends DialectAdapter {
	public OracleStyleFunction() {
	}

	public OracleStyleFunction(SessionFactory session) {
		super(session);
	}

	protected void initAdapter() {
		dialect = new OracleDialectEx();
	}

	public int getCurrentDBType() {
		return ORACLE;
	}

	public String string2Num(String str) {
		List params = new ArrayList();
		params.add(str);
		return execute(getFunc("to_number"), params);
	}

	public String string2Date(String str) {
		List params = new ArrayList();
		params.add(str);
		params.add(FormatUtil.formatStrForDB("yyyy-mm-dd hh24:mi:ss"));
		return execute(getFunc("to_date"), params);
	}

	public String isNull(String str) {
		return "(" + str + " is null or " + str + "='')";
	}

	public String isNotNull(String str) {
		return "(" + str + " is not null or " + str + "<>'')";
	}

	public String string2ShortDate(String str) {
		List params = new ArrayList();
		params.add(str);
		params.add(FormatUtil.formatStrForDB("yyyy-mm-dd"));
		return execute(getFunc("to_date"), params);
	}

	public String string2Date(String str, String format) {
		List params = new ArrayList();
		params.add(str);
		params.add(FormatUtil.formatStrForDB(format));
		return execute(getFunc("to_date"), params);
	}

	public String date2String(String field) {
		List params = new ArrayList();
		params.add(field);
		params.add(FormatUtil.formatStrForDB("yyyy-mm-dd"));
		return execute(getFunc("to_char"), params);
	}

	public String date2String(String field, String format) {
		List params = new ArrayList();
		params.add(field);
		params.add(FormatUtil.formatStrForDB(format));
		return execute(getFunc("to_char"), params);
	}

	public String getUUID() {
		return "sys_guid()";
	}

	public String replaceNull(String field, String val) {
		return "nvl(" + field + "," + val + ")";
	}
}
