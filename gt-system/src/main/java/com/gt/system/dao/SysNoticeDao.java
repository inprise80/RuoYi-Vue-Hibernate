package com.gt.system.dao;

import com.gt.common.core.dao.BaseDao;
import com.gt.common.core.page.Page;
import com.gt.common.core.page.PageDomain;
import com.gt.common.core.page.TableSupport;
import com.gt.common.utils.ValidateUtil;
import com.gt.system.domain.SysNotice;
import org.springframework.stereotype.Repository;

/**
 * 通知公告表 数据层
 *
 * @author liuyj
 */
@Repository
public class SysNoticeDao extends BaseDao<SysNotice,Long> {
    /**
     * 查询公告列表(分页)
     *
     * @param notice 公告信息
     * @return 公告集合
     */
    public Page findNoticeList(SysNotice notice){
        PageDomain pageDomain = TableSupport.buildPageRequest();
        String hql="from SysNotice where 1=1 ";
        if (!ValidateUtil.isEmpty(notice.getNoticeTitle())) {
            hql += " and noticeTitle like '%" + notice.getNoticeTitle() + "%'";
        }
        if (!ValidateUtil.isEmpty(notice.getNoticeType())) {
            hql += " and noticeType= '" + notice.getNoticeType() + "'";
        }
        if (!ValidateUtil.isEmpty(notice.getCreateBy())) {
            hql += " and createBy like '%" + notice.getCreateBy() + "%'";
        }
        return this.findPage(hql, pageDomain.getPageNum(), pageDomain.getPageSize().intValue());
    }
}
