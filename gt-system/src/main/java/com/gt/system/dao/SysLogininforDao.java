package com.gt.system.dao;

import com.gt.common.core.dao.BaseDao;
import com.gt.common.core.dao.dialect.FormatUtil;
import com.gt.common.core.page.Page;
import com.gt.common.core.page.PageDomain;
import com.gt.common.core.page.TableSupport;
import com.gt.common.utils.ValidateUtil;
import com.gt.system.domain.SysLogininfor;
import org.springframework.stereotype.Repository;

/**
 * 系统访问日志情况信息 数据层
 *
 * @author liuyj
 */
@Repository
public class SysLogininforDao extends BaseDao<SysLogininfor,Long> {

    /**
     * 查询系统登录日志集合
     *
     * @param logininfor 访问日志对象
     * @return 登录记录集合
     */
    public Page findLogininforList(SysLogininfor logininfor){
        PageDomain pageDomain = TableSupport.buildPageRequest();
        String hql="from SysLogininfor where 1=1 ";
        if (!ValidateUtil.isEmpty(logininfor.getIpaddr())) {
            hql += " and ipaddr like '%" + logininfor.getIpaddr() + "%'";
        }
        if (!ValidateUtil.isEmpty(logininfor.getUserName())) {
            hql += " and userName like '%" + logininfor.getUserName() + "%'";
        }
        if (!ValidateUtil.isEmpty(logininfor.getStatus())) {
            hql += " and status= '" + logininfor.getStatus() + "'";
        }
        if (!ValidateUtil.isEmpty((String) logininfor.getParams().get("beginTime"))) {
            hql += " and " + sqlUtil.string2ShortDate(sqlUtil.date2String("loginTime"));
            hql += " >= " + sqlUtil.string2ShortDate(FormatUtil.formatStrForDB((String) logininfor.getParams().get("beginTime")));
        }
        if (!ValidateUtil.isEmpty((String) logininfor.getParams().get("endTime"))) {
            hql += " and " + sqlUtil.string2ShortDate(sqlUtil.date2String("loginTime"));
            hql += " <= " + sqlUtil.string2ShortDate(FormatUtil.formatStrForDB((String) logininfor.getParams().get("endTime")));
        }
        hql+=" order by infoId desc";
        return this.findPage(hql, pageDomain.getPageNum(), pageDomain.getPageSize().intValue());
    }

    /**
     * 清空系统登录日志
     *
     * @return 结果
     */
    public int cleanLogininfor(){
        String sql="truncate table sys_logininfor";
        return  this.executeSql(sql);
    }
}
