package com.gt.system.dao;

import com.gt.common.core.dao.BaseDao;
import com.gt.common.core.dao.dialect.FormatUtil;
import com.gt.common.core.domain.entity.SysRole;
import com.gt.common.core.page.Page;
import com.gt.common.core.page.PageDomain;
import com.gt.common.core.page.TableSupport;
import com.gt.common.utils.ConvertUtil;
import com.gt.common.utils.ValidateUtil;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 角色表 数据层
 *
 * @author liuyj
 */
@Repository
public class SysRoleDao extends BaseDao<SysRole,Long> {
    /**
     * 根据条件分页查询角色数据
     *
     * @param role 角色信息
     * @return 角色数据集合信息
     */
    public Page findRoleList(SysRole role){
        PageDomain pageDomain = TableSupport.buildPageRequest();
        String hql="from SysRole where 1=1 ";
        if (role.getRoleId()!=null && role.getRoleId()!=0) {
            hql += " and roleId= "+role.getRoleId();
        }
        if (!ValidateUtil.isEmpty(role.getRoleName())) {
            hql += " and roleName like '%" + role.getRoleName() + "%'";
        }
        if (!ValidateUtil.isEmpty(role.getStatus())) {
            hql += " and status= '" + role.getStatus() + "'";
        }
        if (!ValidateUtil.isEmpty(role.getRoleKey())) {
            hql += " and roleKey like '%" + role.getRoleKey() + "%'";
        }
        if (!ValidateUtil.isEmpty((String) role.getParams().get("beginTime"))) {
            hql += " and " + sqlUtil.string2ShortDate(sqlUtil.date2String("createTime"));
            hql += " >= " + sqlUtil.string2ShortDate(FormatUtil.formatStrForDB((String) role.getParams().get("beginTime")));
        }
        if (!ValidateUtil.isEmpty((String) role.getParams().get("endTime"))) {
            hql += " and " + sqlUtil.string2ShortDate(sqlUtil.date2String("createTime"));
            hql += " <= " + sqlUtil.string2ShortDate(FormatUtil.formatStrForDB((String) role.getParams().get("endTime")));
        }
        hql+=" order by roleSort";
        return this.findPage(hql, pageDomain.getPageNum(), pageDomain.getPageSize().intValue());
    }

    /**
     * 根据用户ID查询角色
     *
     * @param userId 用户ID
     * @return 角色列表
     */
    public List<SysRole> findRolePermissionByUserId(Long userId){
        StringBuilder sql = new StringBuilder();
        sql.append(" select ");
        sql.append(" distinct r.role_id , r.role_name , r.role_key , r.role_sort , ");
        sql.append(" r.data_scope , r.menu_check_strictly , r.dept_check_strictly , ");
        sql.append(" r.status , r.del_flag , r.create_time , r.remark ,r.create_by ,r.update_by ,r.update_time  ");
        sql.append(" from sys_role r ");
        sql.append(" left join sys_user_role ur on ur.role_id = r.role_id ");
        sql.append(" left join sys_user u on u.user_id = ur.user_id ");
        sql.append(" left join sys_dept d on u.dept_id = d.dept_id ");
        sql.append(" WHERE r.del_flag = '0' and ur.user_id = ");
        sql.append(userId);
        return findBySQL(sql.toString());
    }

    /**
     * 查询所有角色
     *
     * @return 角色列表
     */
    public List<SysRole> findRoleAll(){
        String hql="from SysRole where delFlag='0'";
        return find(hql);
    }

    /**
     * 根据用户ID获取角色选择框列表
     *
     * @param userId 用户ID
     * @return 选中角色ID列表
     */
    public List<Long> findRoleListByUserId(Long userId){
        String sql=" select r.role_id    " +
                " from sys_role r  " +
                " left join sys_user_role ur on ur.role_id = r.role_id   " +
                " left join sys_user u on u.user_id = ur.user_id  " +
                " where u.user_id =  "+userId;
        return this.findByFreeSQL(sql);
    }

    /**
     * 根据用户ID查询角色
     *
     * @param userName 用户名
     * @return 角色列表
     */
    public List<SysRole> findRolesByUserName(String userName){
        StringBuilder sql = new StringBuilder();
        sql.append(" select ");
        /**
        sql.append(" distinct r.role_id roleId, r.role_name roleName, r.role_key roleKey, r.role_sort roleSort, ");
        sql.append(" r.data_scope dataScope, r.menu_check_strictly menuCheckStrictly, r.dept_check_strictly deptCheckStrictly, ");
        sql.append(" r.status status, r.del_flag delFlag,r.create_by createBy,r.create_time createTime, r.remark remark,r.update_by updateBy,r.update_time updateTime ");
         */
        sql.append(" distinct r.role_id,r.role_name,r.role_key,r.role_sort, ");
        sql.append(" r.data_scope,r.menu_check_strictly,r.dept_check_strictly, ");
        sql.append(" r.status,r.del_flag,r.create_by,r.create_time, r.remark,r.update_by,r.update_time ");
        sql.append(" from sys_role r ");
        sql.append(" left join sys_user_role ur on ur.role_id = r.role_id ");
        sql.append(" left join sys_user u on u.user_id = ur.user_id ");
        sql.append(" left join sys_dept d on u.dept_id = d.dept_id ");
        sql.append(" WHERE r.del_flag = '0' and u.user_name  = '");
        sql.append(userName);
        sql.append("'");
        return this.findBySQL(sql.toString());
        //return this.findByFreeSQL(sql.toString(),SysRole.class);
    }

    /**
     * 校验角色名称是否唯一
     *
     * @param roleName 角色名称
     * @return 角色信息
     */
    public SysRole checkRoleNameUnique(String roleName){
        SysRole sysRole=null;
        String hql="from SysRole where roleName='"+roleName+"'";
        List<SysRole> list=this.find(hql);
        if(!ValidateUtil.isEmpty(list)){
            sysRole=list.get(0);
        }
        return sysRole;
    }

    /**
     * 校验角色权限是否唯一
     *
     * @param roleKey 角色权限
     * @return 角色信息
     */
    public SysRole checkRoleKeyUnique(String roleKey){
        SysRole sysRole=null;
        String hql="from SysRole where roleKey='"+roleKey+"'";
        List<SysRole> list=this.find(hql);
        if(!ValidateUtil.isEmpty(list)){
            sysRole=list.get(0);
        }
        return sysRole;
    }

    /**
     * 根据角色ID数组查询角色
     *
     * @param roleIds 角色ID数组
     * @return 角色列表
     */
    public List<SysRole> findByRoleIdIn(Long[] roleIds){
        String hql="from SysRole where roleId in "+ ConvertUtil.toDbString(roleIds);
        return this.find(hql);
    }

}
