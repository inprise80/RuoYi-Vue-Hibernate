package com.gt.system.dao;

import com.gt.common.core.dao.BaseDao;
import com.gt.common.core.dao.dialect.FormatUtil;
import com.gt.common.core.page.Page;
import com.gt.common.core.page.PageDomain;
import com.gt.common.core.page.TableSupport;
import com.gt.common.utils.ValidateUtil;
import com.gt.system.domain.SysOperLog;
import org.springframework.stereotype.Repository;

/**
 * 操作日志 数据层
 *
 * @author liuyj
 */
@Repository
public class SysOperLogDao extends BaseDao<SysOperLog,Long> {
    /**
     * 查询系统操作日志集合
     *
     * @param operLog 操作日志对象
     * @return 操作日志集合
     */
    public Page findOperLogList(SysOperLog operLog){
        PageDomain pageDomain = TableSupport.buildPageRequest();
        String hql="from SysOperLog where 1=1 ";
        if (!ValidateUtil.isEmpty(operLog.getTitle())) {
            hql += " and title like '%" + operLog.getTitle() + "%'";
        }
        if (operLog.getBusinessType()!=null && operLog.getBusinessType()!=0) {
            hql += " and businessType= '" + operLog.getBusinessType() + "'";
        }
        if (operLog.getStatus()!=null) {
            hql += " and status= " + operLog.getStatus() ;
        }
        if (!ValidateUtil.isEmpty(operLog.getOperName())) {
            hql += " and operName like '%" + operLog.getOperName() + "%'";
        }
        if (!ValidateUtil.isEmpty((String) operLog.getParams().get("beginTime"))) {
            hql += " and " + sqlUtil.string2ShortDate(sqlUtil.date2String("operTime"));
            hql += " >= " + sqlUtil.string2ShortDate(FormatUtil.formatStrForDB((String) operLog.getParams().get("beginTime")));
        }
        if (!ValidateUtil.isEmpty((String) operLog.getParams().get("endTime"))) {
            hql += " and " + sqlUtil.string2ShortDate(sqlUtil.date2String("operTime"));
            hql += " <= " + sqlUtil.string2ShortDate(FormatUtil.formatStrForDB((String) operLog.getParams().get("endTime")));
        }
        return this.findPage(hql, pageDomain.getPageNum(), pageDomain.getPageSize().intValue());
    }
    /**
     * 清空操作日志
     */
    public void cleanOperLog(){
        String sql="truncate table sys_oper_log";
        executeSql(sql);
    }
}
