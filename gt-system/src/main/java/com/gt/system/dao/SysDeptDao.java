package com.gt.system.dao;

import com.gt.common.core.dao.BaseDao;
import com.gt.common.core.dao.dialect.FormatUtil;
import com.gt.common.core.domain.entity.SysDept;
import com.gt.common.utils.ConvertUtil;
import com.gt.common.utils.ValidateUtil;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 部门管理 数据层
 *
 * @author liuyj
 */
@Repository
public class SysDeptDao extends BaseDao<SysDept, Long> {
    /**
     * 查询部门管理数据
     *
     * @param dept 部门信息
     * @return 部门信息集合
     */
    public List<SysDept> findDeptList(SysDept dept){
        String hql="from SysDept where delFlag='0' ";
        if (dept.getDeptId()!=null && dept.getDeptId()!=0) {
            hql += " and deptIdId= "+dept.getDeptId();
        }
        if (dept.getParentId()!=null && dept.getParentId()!=0) {
            hql += " and parentId= "+dept.getParentId();
        }
        if (!ValidateUtil.isEmpty(dept.getDeptName())) {
            hql += " and deptName like '%" + dept.getDeptName() + "%'";
        }
        if (!ValidateUtil.isEmpty(dept.getStatus())) {
            hql += " and status= "+dept.getStatus();
        }
        return this.find(hql);
    }

    /**
     * 根据角色ID查询部门树信息
     *
     * @param roleId 角色ID
     * @param deptCheckStrictly 部门树选择项是否关联显示
     * @return 选中部门列表
     */
    public List<Long> findDeptListByRoleId(Long roleId, boolean deptCheckStrictly){
        StringBuilder hql = new StringBuilder();
        hql.append(" select  d.deptId from SysDept d ");
        hql.append(" left join SysRoleDept rd on d.deptId = rd.deptId ");
        hql.append(" where rd.roleId = ?1");
        if (deptCheckStrictly) {
            hql.append(" and d.deptId not in (select d.parentId from SysDept d inner join SysRoleDept rd on d.deptId = rd.deptId and rd.roleId = ?2").append(" )");
        }
        hql.append(" order by d.parentId desc, d.orderNum desc ");
        Object param[] = new Object[2];
        param[0] = roleId;
        param[1] = roleId;
        return this.find(hql.toString(), param);
    }

    /**
     * 根据ID查询所有子部门
     *
     * @param deptId 部门ID
     * @return 部门列表
     */
    public List<SysDept> findChildrenDeptById(Long deptId){
        String sql="select * from sys_dept where find_in_set("+deptId+", ancestors)";
        return this.findBySQL(sql);
    }

    /**
     * 根据ID查询所有子部门（正常状态）
     *
     * @param deptId 部门ID
     * @return 子部门数
     */
    public int selectNormalChildrenDeptById(Long deptId){
        String sql="select count(1) from sys_dept where status = 0 and del_flag = '0' and find_in_set("+deptId+", ancestors)";
        List list=this.findByFreeSQL(sql);
        int total = ((Long) list.get(0)).intValue();
        return  total;
    }

    /**
     * 是否存在子节点
     *
     * @param deptId 部门ID
     * @return 结果
     */
    public int hasChildByDeptId(Long deptId){
        String hql = "select count(*) from SysDept  where delFlag = '0' and parentId=" + deptId;
        Long total = (Long) this.find(hql).get(0);
        return total.intValue();
    }

    /**
     * 查询部门是否存在用户
     *
     * @param deptId 部门ID
     * @return 结果
     */
    public int checkDeptExistUser(Long deptId){
        return 1;
    }

    /**
     * 通过部门名称与父部门ID查询
     *
     * @param deptName 部门名称
     * @param parentId 父部门ID
     * @return 结果
     */
    public List<SysDept> findByParentIdAndDeptName(String deptName,Long parentId){
        String hql="from SysDept where parentId="+parentId+" and deptName="+ FormatUtil.formatStrForDB(deptName);
        return find(hql);
    }


    /**
     * 修改所在部门正常状态
     *
     * @param deptIds 部门ID组
     */
    public void updateDeptStatusNormal(Long[] deptIds){
        String hql="update SysDept set status = '0' where deptId in "+ ConvertUtil.toDbString(deptIds);
        this.executeHql(hql);
    }

    /**
     * 修改子元素关系
     *
     * @param depts 子元素
     * @return 结果
     */
    public int updateDeptChildren(List<SysDept> depts){
        return 1;
    }


}
