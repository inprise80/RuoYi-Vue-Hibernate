package com.gt.system.dao;

import com.gt.common.core.dao.BaseDao;
import com.gt.common.utils.ConvertUtil;
import com.gt.system.domain.SysUserPost;
import com.gt.system.domain.groupkey.SysUserPostKey;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 用户与岗位关联表 数据层
 *
 * @author liuyj
 */
@Repository
public class SysUserPostDao extends BaseDao<SysUserPost, SysUserPostKey> {
    /**
     * 通过用户ID删除用户和岗位关联
     *
     * @param userId 用户ID
     * @return 结果
     */
    public int deleteUserPostByUserId(Long userId){
        String hql="delete from SysUserPost where userId="+userId;
        return this.executeHql(hql);
    }

    /**
     * 通过岗位ID查询岗位使用数量
     *
     * @param postId 岗位ID
     * @return 结果
     */
    public int countUserPostById(Long postId){
        String hql = "select count(1) from SysUserPost  where postId ="+ postId;
        Long total = (Long) this.find(hql).get(0);
        return total.intValue();
    }

    /**
     * 批量删除用户和岗位关联
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteUserPost(Long[] ids){
        String hql = "delete from SysUserPost  where userId in "+ ConvertUtil.toDbString(ids);
        return this.executeHql(hql);
    }

    public List<SysUserPost> findByUserId(Long userId){
        String hql="from SysUserPost where userId="+userId;
        return  this.find(hql);
    }

}
