package com.gt.system.dao;

import com.gt.common.core.dao.BaseDao;
import com.gt.common.utils.ConvertUtil;
import com.gt.system.domain.SysRoleMenu;
import com.gt.system.domain.groupkey.SysRoleMenuKey;
import org.springframework.stereotype.Repository;

/**
 * 角色与菜单关联表 数据层
 *
 * @author liuyj
 */
@Repository
public class SysRoleMenuDao extends BaseDao<SysRoleMenu, SysRoleMenuKey> {
    /**
     * 查询菜单使用数量
     *
     * @param menuId 菜单ID
     * @return 结果
     */
    public int checkMenuExistRole(Long menuId){
        String hql = "select count(1) from SysRoleMenu  where menuId ="+menuId;
        Long total = (Long) this.find(hql).get(0);
        return total.intValue();
    }

    /**
     * 通过角色ID删除角色和菜单关联
     *
     * @param roleId 角色ID
     * @return 结果
     */
    public int deleteRoleMenuByRoleId(Long roleId){
        String hql="delete from SysRoleMenu where roleId="+roleId;
        return this.executeHql(hql);
    }

    /**
     * 批量删除角色菜单关联信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteRoleMenu(Long[] ids){
        String hql="delete from SysRoleMenu where roleId in "+ ConvertUtil.toDbString(ids);
        return this.executeHql(hql);
    }

}
