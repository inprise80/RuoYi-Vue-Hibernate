package com.gt.system.dao;

import com.gt.common.core.dao.BaseDao;
import com.gt.common.core.dao.dialect.FormatUtil;
import com.gt.common.core.domain.entity.SysUser;
import com.gt.common.core.dto.SysUserDto;
import com.gt.common.core.page.Page;
import com.gt.common.core.page.PageDomain;
import com.gt.common.core.page.TableSupport;
import com.gt.common.utils.ValidateUtil;
import com.gt.common.utils.bean.BeanUtils;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 用户表 数据层
 *
 * @author liuyj
 */
@Repository
public class SysUserDao extends BaseDao<SysUser,Long> {
    /**
     * 根据条件分页查询用户列表
     *
     * @param sysUser 用户信息
     * @return 用户信息集合信息
     */
    public Page findUserList(SysUser sysUser){
        PageDomain pageDomain = TableSupport.buildPageRequest();
        String hql="from SysUser where 1=1 ";
        if (sysUser.getUserId()!=null && sysUser.getUserId()!=0) {
            hql += " and userId= "+sysUser.getUserId();
        }
        if (!ValidateUtil.isEmpty(sysUser.getUserName())) {
            hql += " and userName like '%" + sysUser.getUserName() + "%'";
        }
        if (!ValidateUtil.isEmpty(sysUser.getStatus())) {
            hql += " and status= "+sysUser.getStatus();
        }
        if (!ValidateUtil.isEmpty(sysUser.getPhonenumber())) {
            hql += " and phonenumber like '%" + sysUser.getPhonenumber() + "%'";
        }
        if (sysUser.getDeptId()!=null && sysUser.getDeptId()!=0) {
            hql += " and deptId= "+sysUser.getDeptId();
        }
        if (!ValidateUtil.isEmpty((String) sysUser.getParams().get("beginTime"))) {
            hql += " and " + sqlUtil.string2ShortDate(sqlUtil.date2String("createTime"));
            hql += " >= " + sqlUtil.string2ShortDate(FormatUtil.formatStrForDB((String) sysUser.getParams().get("beginTime")));
        }
        if (!ValidateUtil.isEmpty((String) sysUser.getParams().get("endTime"))) {
            hql += " and " + sqlUtil.string2ShortDate(sqlUtil.date2String("createTime"));
            hql += " <= " + sqlUtil.string2ShortDate(FormatUtil.formatStrForDB((String) sysUser.getParams().get("endTime")));
        }
        return this.findPage(hql, pageDomain.getPageNum(), pageDomain.getPageSize().intValue());
    }

    /**
     * 根据条件分页查询已配用户角色列表
     *
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    public Page findAllocatedList(SysUser user){
        PageDomain pageDomain = TableSupport.buildPageRequest();
        String sql="select distinct u.user_id as userId, u.dept_id as deptId, u.user_name as userName, u.nick_name as nickName, u.email as email, u.phonenumber as phonenumber, u.status as status, u.create_time as createTime " +
                   " from sys_user u " +
                   " left join sys_dept d on u.dept_id = d.dept_id " +
                   " left join sys_user_role ur on u.user_id = ur.user_id " +
                   " left join sys_role r on r.role_id = ur.role_id " +
                   " where u.del_flag = '0' and r.role_id = "+user.getRoleId();
        if (!ValidateUtil.isEmpty(user.getUserName())) {
            sql += " and u.user_name like '%" + user.getUserName() + "%'";
        }
        if (!ValidateUtil.isEmpty(user.getPhonenumber())) {
            sql += " and u.phonenumber like '%" + user.getPhonenumber() + "%'";
        }
        Page page=this.findPageByFreeSQL(sql,pageDomain.getPageNum(), pageDomain.getPageSize().intValue(), SysUserDto.class);
        /**
        List<SysUserDto> itemList = page.getItems();
        List<SysUser> collectList = itemList.stream().map(s -> {
            SysUser sysUser = new SysUser();
            BeanUtils.copyProperties(s, sysUser);
            sysUser.setUserId(s.getUserId().longValue());
            sysUser.setDeptId(s.getDeptId().longValue());
            sysUser.setStatus(s.getStatus().toString());
            return sysUser;
        }).collect(Collectors.toList());
         */
        return page;
    }


    /**
     * 根据条件分页查询未分配用户角色列表
     *
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    public Page findUnallocatedList(SysUser user){
        PageDomain pageDomain = TableSupport.buildPageRequest();
        String sql="select distinct u.user_id as userId, u.dept_id as deptId, u.user_name as userName, u.nick_name as nickName, u.email as email, u.phonenumber as phonenumber, u.status as status, u.create_time as createTime " +
                " from sys_user u " +
                " left join sys_dept d on u.dept_id = d.dept_id " +
                " left join sys_user_role ur on u.user_id = ur.user_id " +
                " left join sys_role r on r.role_id = ur.role_id " +
                " where u.del_flag = '0' and (r.role_id != "+user.getRoleId()+" or r.role_id IS NULL)"+
                " and u.user_id not in (select u.user_id from sys_user u inner join sys_user_role ur on u.user_id = ur.user_id and ur.role_id = "+user.getRoleId()+")";
        if (!ValidateUtil.isEmpty(user.getUserName())) {
            sql += " and u.user_name like '%" + user.getUserName() + "%'";
        }
        if (!ValidateUtil.isEmpty(user.getPhonenumber())) {
            sql += " and u.phonenumber like '%" + user.getPhonenumber() + "%'";
        }
        return this.findPageByFreeSQL(sql,pageDomain.getPageNum(), pageDomain.getPageSize().intValue(), SysUserDto.class);
    }

    /**
     * 通过用户名查询用户
     *
     * @param userName 用户名
     * @return 用户对象信息
     */
    public SysUser selectUserByUserName(String userName){
        SysUser sysUser=null;
        String hql="from SysUser where userName="+FormatUtil.formatStrForDB(userName);
        List<SysUser> list=this.find(hql);
        if(!ValidateUtil.isEmpty(list)){
            sysUser=list.get(0);
        }
        return  sysUser;
    }

    /**
     * 修改用户头像
     *
     * @param userName 用户名
     * @param avatar 头像地址
     * @return 结果
     */
    public int updateUserAvatar(String userName,String avatar){
        String hql="update SysUser set avatar="+FormatUtil.formatStrForDB(avatar)+" where userName="+FormatUtil.formatStrForDB(userName);
        return this.executeHql(hql);
    }

    /**
     * 重置用户密码
     *
     * @param userName 用户名
     * @param password 密码
     * @return 结果
     */
    public int resetUserPwd(String userName, String password){
        String hql="update SysUser set password="+FormatUtil.formatStrForDB(password)+" where userName="+FormatUtil.formatStrForDB(userName);
        return this.executeHql(hql);
    }

    /**
     * 校验用户名称是否唯一
     *
     * @param userName 用户名称
     * @return 结果
     */
    public int checkUserNameUnique(String userName){
        String hql = "select count(1) from SysUser  where userName ="+FormatUtil.formatStrForDB(userName);
        Long total = (Long) this.find(hql).get(0);
        return total.intValue();
    }

    /**
     * 校验手机号码是否唯一
     *
     * @param phonenumber 手机号码
     * @return 结果
     */
    public SysUser checkPhoneUnique(String phonenumber){
        SysUser sysUser=null;
        String hql="from SysUser where phonenumber=?1";
        List<SysUser> list=this.find(hql,phonenumber);
        if(!ValidateUtil.isEmpty(list)){
            sysUser=list.get(0);
        }
        return sysUser;
    }

    /**
     * 校验email是否唯一
     *
     * @param email 用户邮箱
     * @return 结果
     */
    public SysUser checkEmailUnique(String email){
        SysUser sysUser=null;
        String hql="from SysUser where email=?1";
        List<SysUser> list=this.find(hql,email);
        if(!ValidateUtil.isEmpty(list)){
            sysUser=list.get(0);
        }
        return sysUser;
    }
}
