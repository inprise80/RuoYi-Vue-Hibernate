package com.gt.system.dao;

import com.gt.common.core.dao.BaseDao;
import com.gt.common.core.page.Page;
import com.gt.common.core.page.PageDomain;
import com.gt.common.core.page.TableSupport;
import com.gt.common.utils.ValidateUtil;
import com.gt.system.domain.SysPost;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 岗位信息 数据层
 *
 * @author liuyj
 */
@Repository
public class SysPostDao extends BaseDao<SysPost,Long> {
    /**
     * 查询岗位数据集合(分页)
     *
     * @param post 岗位信息
     * @return 岗位数据集合
     */
    public Page findPostList(SysPost post){
        PageDomain pageDomain = TableSupport.buildPageRequest();
        String hql="from SysPost where 1=1 ";
        if (!ValidateUtil.isEmpty(post.getPostCode())) {
            hql += " and postCode like '%" + post.getPostCode() + "%'";
        }
        if (!ValidateUtil.isEmpty(post.getStatus())) {
            hql += " and status= '" + post.getStatus()+"'" ;
        }
        if (!ValidateUtil.isEmpty(post.getPostName())) {
            hql += " and postName like '%" + post.getPostName() + "%'";
        }
        return this.findPage(hql, pageDomain.getPageNum(), pageDomain.getPageSize().intValue());
    }

    /**
     * 根据用户ID获取岗位选择框列表
     *
     * @param userId 用户ID
     * @return 选中岗位ID列表
     */
    public List<Long> findPostListByUserId(Long userId){
        StringBuilder sql = new StringBuilder();
        sql.append(" select p.post_id ");
        sql.append(" from sys_post p ");
        sql.append(" left join sys_user_post up on up.post_id = p.post_id ");
        sql.append(" left join sys_user u on u.user_id = up.user_id ");
        sql.append(" where u.user_id =  ");
        sql.append(userId);
        return this.findByFreeSQL(sql.toString());
    }

    /**
     * 查询用户所属岗位组
     *
     * @param userName 用户名
     * @return 结果
     */
    public List<SysPost> findPostsByUserName(String userName){
        StringBuilder sql = new StringBuilder();
        sql.append(" select p.* ");
        sql.append(" from sys_post p ");
        sql.append(" left join sys_user_post up on up.post_id = p.post_id ");
        sql.append(" left join sys_user u on u.user_id = up.user_id ");
        sql.append(" where u.user_name ='");
        sql.append(userName);
        sql.append("'");
        return this.findBySQL(sql.toString());
    }


    /**
     * 校验岗位名称
     *
     * @param postName 岗位名称
     * @return 结果
     */
    public SysPost checkPostNameUnique(String postName){
        SysPost sysPost=null;
        String hql="from SysPost where postName=?1";
        List<SysPost> list=find(hql,postName);
        if(!ValidateUtil.isEmpty(list)){
            sysPost=list.get(0);
        }
        return sysPost;
    }

    /**
     * 校验岗位编码
     *
     * @param postCode 岗位编码
     * @return 结果
     */
    public SysPost checkPostCodeUnique(String postCode){
        SysPost sysPost=null;
        String hql="from SysPost where postCode=?1";
        List<SysPost> list=find(hql,postCode);
        if(!ValidateUtil.isEmpty(list)){
            sysPost=list.get(0);
        }
        return sysPost;
    }
}
