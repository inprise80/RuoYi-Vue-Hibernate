package com.gt.system.dao;

import com.gt.common.core.dao.BaseDao;
import com.gt.common.core.dao.dialect.FormatUtil;
import com.gt.common.core.domain.entity.SysDictType;
import com.gt.common.core.page.Page;
import com.gt.common.core.page.PageDomain;
import com.gt.common.core.page.TableSupport;
import com.gt.common.utils.ValidateUtil;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 字典表 数据层
 *
 * @author liuyj
 */
@Repository
public class SysDictTypeDao extends BaseDao<SysDictType,Long> {
    /**
     * 根据条件分页查询字典类型
     *
     * @param dictType 字典类型信息
     * @return 字典类型集合信息
     */
    public Page findDictTypeList(SysDictType dictType){
        PageDomain pageDomain = TableSupport.buildPageRequest();
        String hql="from SysDictType where 1=1 ";
        if (!ValidateUtil.isEmpty(dictType.getDictName())) {
            hql += " and dictName like '%" + dictType.getDictName() + "%'";
        }
        if (!ValidateUtil.isEmpty(dictType.getStatus())) {
            hql += " and status= '" + dictType.getStatus() + "'";
        }
        if (!ValidateUtil.isEmpty(dictType.getDictType())) {
            hql += " and dictType like '%" + dictType.getDictType() + "%'";
        }
        if (!ValidateUtil.isEmpty((String) dictType.getParams().get("beginTime"))) {
            hql += " and " + sqlUtil.string2ShortDate(sqlUtil.date2String("createTime"));
            hql += " >= " + sqlUtil.string2ShortDate(FormatUtil.formatStrForDB((String) dictType.getParams().get("beginTime")));
        }
        if (!ValidateUtil.isEmpty((String) dictType.getParams().get("endTime"))) {
            hql += " and " + sqlUtil.string2ShortDate(sqlUtil.date2String("createTime"));
            hql += " <= " + sqlUtil.string2ShortDate(FormatUtil.formatStrForDB((String) dictType.getParams().get("endTime")));
        }
        return this.findPage(hql, pageDomain.getPageNum(), pageDomain.getPageSize().intValue());
    }

    /**
     * 根据字典类型查询信息
     *
     * @param dictType 字典类型
     * @return 字典类型
     */
    public SysDictType selectDictTypeByType(String dictType){
        String hql="from SysDictType where dictType=?1 ";
        List<SysDictType> list=find(hql,dictType);
        SysDictType type=null;
        if(!ValidateUtil.isEmpty(list)){
            type=list.get(0);
        }
        return type;
    }

}
