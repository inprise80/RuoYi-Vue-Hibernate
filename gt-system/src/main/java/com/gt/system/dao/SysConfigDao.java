package com.gt.system.dao;

import com.gt.common.core.dao.BaseDao;
import com.gt.common.core.dao.dialect.FormatUtil;
import com.gt.common.core.page.Page;
import com.gt.common.core.page.PageDomain;
import com.gt.common.core.page.TableSupport;
import com.gt.common.utils.ValidateUtil;
import com.gt.system.domain.SysConfig;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 参数配置DAO层
 *
 * @author liuyj
 */
@Repository
public class SysConfigDao extends BaseDao<SysConfig, Long> {
    public SysConfigDao() {
        modelClass = SysConfig.class;
        super.defaultOrder = "";
    }
    /**
     * 查询参数配置信息
     *
     * @param config 参数配置信息
     * @return 参数配置信息
     */
    public SysConfig selectConfig(SysConfig config){
        String hql="from SysConfig where 1=1 ";
        if (config.getConfigId()!=null) {
            hql += " and configId= "+config.getConfigId();
        }
        if (!ValidateUtil.isEmpty(config.getConfigKey())) {
            hql += " and configKey= '" + config.getConfigKey() + "'";
        }
        List<SysConfig> list=find(hql);
        if(!ValidateUtil.isEmpty(list)){
            return list.get(0);
        }
        return null;
    }

    /**
     * 查询参数配置列表(分页)
     *
     * @param config 参数配置信息
     * @return 参数配置集合
     */
    public Page findConfigList(SysConfig config){
        PageDomain pageDomain = TableSupport.buildPageRequest();
        String hql="from SysConfig where 1=1 ";
        if (!ValidateUtil.isEmpty(config.getConfigName())) {
            hql += " and configName like '%" + config.getConfigName() + "%'";
        }
        if (!ValidateUtil.isEmpty(config.getConfigType())) {
            hql += " and configType= '" + config.getConfigType() + "'";
        }
        if (!ValidateUtil.isEmpty(config.getConfigKey())) {
            hql += " and configKey like '%" + config.getConfigKey() + "%'";
        }
        if (!ValidateUtil.isEmpty((String) config.getParams().get("beginTime"))) {
            hql += " and " + sqlUtil.string2ShortDate(sqlUtil.date2String("createTime"));
            hql += " >= " + sqlUtil.string2ShortDate(FormatUtil.formatStrForDB((String) config.getParams().get("beginTime")));
        }
        if (!ValidateUtil.isEmpty((String) config.getParams().get("endTime"))) {
            hql += " and " + sqlUtil.string2ShortDate(sqlUtil.date2String("createTime"));
            hql += " <= " + sqlUtil.string2ShortDate(FormatUtil.formatStrForDB((String) config.getParams().get("endTime")));
        }
        return this.findPage(hql, pageDomain.getPageNum(), pageDomain.getPageSize().intValue());
    }

    /**
     * 根据键名查询参数配置信息
     *
     * @param configKey 参数键名
     * @return 参数配置List
     */
    public List<SysConfig> findByConfigKey(String configKey){
        String hql="from SysConfig where configKey=?1 ";
        List<SysConfig> list=find(hql,configKey);
        return list;
    }
}
