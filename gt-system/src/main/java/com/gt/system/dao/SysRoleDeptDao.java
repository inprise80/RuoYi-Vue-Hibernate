package com.gt.system.dao;

import com.gt.common.core.dao.BaseDao;
import com.gt.common.utils.ConvertUtil;
import com.gt.system.domain.SysRoleDept;
import com.gt.system.domain.groupkey.SysRoleDeptKey;
import org.springframework.stereotype.Repository;

/**
 * 角色与部门关联表 数据层
 *
 * @author liuyj
 */
@Repository
public class SysRoleDeptDao extends BaseDao<SysRoleDept, SysRoleDeptKey> {
    /**
     * 通过角色ID删除角色和部门关联
     *
     * @param roleId 角色ID
     * @return 结果
     */
    public int deleteRoleDeptByRoleId(Long roleId){
        String hql="delete from SysRoleDept where roleId="+roleId;
        return this.executeHql(hql);
    }

    /**
     * 批量删除角色部门关联信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteRoleDept(Long[] ids){
        String hql="delete from SysRoleDept where roleId in "+ ConvertUtil.toDbString(ids);
        return this.executeHql(hql);
    }

    /**
     * 查询部门使用数量
     *
     * @param deptId 部门ID
     * @return 结果
     */
    public int selectCountRoleDeptByDeptId(Long deptId){
        String hql = "select count(1) from SysRoleDept  where deptId ="+deptId;
        Long total = (Long) this.find(hql).get(0);
        return total.intValue();
    }

}
