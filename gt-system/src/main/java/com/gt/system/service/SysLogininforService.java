package com.gt.system.service;

import com.gt.common.core.dao.BaseDao;
import com.gt.common.core.page.Page;
import com.gt.common.core.service.BaseService;
import com.gt.system.dao.SysLogininforDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.gt.system.domain.SysLogininfor;
import org.springframework.transaction.annotation.Transactional;

/**
 * 系统访问日志情况信息 服务层处理
 * 
 * @author liuyj
 */
@Service
@Transactional
public class SysLogininforService extends BaseService<SysLogininfor,Long>
{

    @Autowired
    private SysLogininforDao sysLogininforDao;
    @Override
    protected BaseDao<SysLogininfor, Long> getDao() {
        return sysLogininforDao;
    }

    /**
     * 查询系统登录日志集合（分页）
     * 
     * @param logininfor 访问日志对象
     * @return 登录记录集合
     */
    public Page findLogininforList(SysLogininfor logininfor)
    {
        return sysLogininforDao.findLogininforList(logininfor);
    }

    /**
     * 保存登录日志
     * @param logininfor 日志对象
     */
    public void saveData(SysLogininfor logininfor){
        sysLogininforDao.saveObject(logininfor);
    }


    /**
     * 清空系统登录日志
     */
    public void cleanLogininfor()
    {
        sysLogininforDao.cleanLogininfor();
    }


}
