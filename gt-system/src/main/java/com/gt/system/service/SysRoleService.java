package com.gt.system.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.gt.common.core.dao.BaseDao;
import com.gt.common.core.page.Page;
import com.gt.common.core.service.BaseService;
import com.gt.system.dao.SysRoleDao;
import com.gt.system.dao.SysRoleDeptDao;
import com.gt.system.dao.SysRoleMenuDao;
import com.gt.system.dao.SysUserRoleDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.gt.common.annotation.DataScope;
import com.gt.common.constant.UserConstants;
import com.gt.common.core.domain.entity.SysRole;
import com.gt.common.core.domain.entity.SysUser;
import com.gt.common.exception.ServiceException;
import com.gt.common.utils.SecurityUtils;
import com.gt.common.utils.StringUtils;
import com.gt.system.domain.SysRoleDept;
import com.gt.system.domain.SysRoleMenu;
import com.gt.system.domain.SysUserRole;

import javax.annotation.Resource;

/**
 * 角色 业务层处理
 * 
 * @author liuyj
 */
@Service
public class SysRoleService extends BaseService
{
    @Autowired
    private SysRoleDao sysRoleDao;

    @Resource
    private SysRoleMenuDao sysRoleMenuDao;

    @Resource
    private SysUserRoleDao sysUserRoleDao;

    @Resource
    private SysRoleDeptDao sysRoleDeptDao;
    @Override
    protected BaseDao getDao() {
        return sysRoleDao;
    }

    /**
     * 根据条件分页查询角色数据
     * 
     * @param role 角色信息
     * @return 角色数据集合信息
     */
    @DataScope(deptAlias = "d")
    public Page findRoleList(SysRole role)
    {
        return sysRoleDao.findRoleList(role);
    }

    /**
     * 根据用户ID查询角色
     * 
     * @param userId 用户ID
     * @return 角色列表
     */
    public List<SysRole> findRolesByUserId(Long userId)
    {
        List<SysRole> userRoles = sysRoleDao.findRolePermissionByUserId(userId);
        List<SysRole> roles = findRoleAll();
        for (SysRole role : roles)
        {
            for (SysRole userRole : userRoles)
            {
                if (role.getRoleId().longValue() == userRole.getRoleId().longValue())
                {
                    role.setFlag(true);
                    break;
                }
            }
        }
        return roles;
    }

    /**
     * 根据用户ID查询权限
     * 
     * @param userId 用户ID
     * @return 权限列表
     */
    public Set<String> findRolePermissionByUserId(Long userId)
    {
        List<SysRole> perms = sysRoleDao.findRolePermissionByUserId(userId);
        Set<String> permsSet = new HashSet<>();
        for (SysRole perm : perms)
        {
            if (StringUtils.isNotNull(perm))
            {
                permsSet.addAll(Arrays.asList(perm.getRoleKey().trim().split(",")));
            }
        }
        return permsSet;
    }

    /**
     * 查询所有角色
     * 
     * @return 角色列表
     */
    public List<SysRole> findRoleAll()
    {
        return sysRoleDao.findRoleAll();
    }

    /**
     * 根据用户ID获取角色选择框列表
     * 
     * @param userId 用户ID
     * @return 选中角色ID列表
     */
    public List<Long> findRoleListByUserId(Long userId)
    {
        return sysRoleDao.findRoleListByUserId(userId);
    }


    /**
     * 校验角色名称是否唯一
     * 
     * @param role 角色信息
     * @return 结果
     */
    public String checkRoleNameUnique(SysRole role)
    {
        Long roleId = StringUtils.isNull(role.getRoleId()) ? -1L : role.getRoleId();
        SysRole info = sysRoleDao.checkRoleNameUnique(role.getRoleName());
        if (StringUtils.isNotNull(info) && info.getRoleId().longValue() != roleId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验角色权限是否唯一
     * 
     * @param role 角色信息
     * @return 结果
     */
    public String checkRoleKeyUnique(SysRole role)
    {
        Long roleId = StringUtils.isNull(role.getRoleId()) ? -1L : role.getRoleId();
        SysRole info = sysRoleDao.checkRoleKeyUnique(role.getRoleKey());
        if (StringUtils.isNotNull(info) && info.getRoleId().longValue() != roleId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验角色是否允许操作
     * 
     * @param role 角色信息
     */
    public void checkRoleAllowed(SysRole role)
    {
        if (StringUtils.isNotNull(role.getRoleId()) && role.isAdmin())
        {
            throw new ServiceException("不允许操作超级管理员角色");
        }
    }

    /**
     * 校验角色是否有数据权限
     * 
     * @param roleId 角色id
     */
    public void checkRoleDataScope(Long roleId)
    {
        if (!SysUser.isAdmin(SecurityUtils.getUserId()))
        {
            SysRole role = new SysRole();
            role.setRoleId(roleId);
            //List<SysRole> roles = SpringUtils.getAopProxy(this).findRoleList(role);
            //if (StringUtils.isEmpty(roles))
            //{
            //    throw new ServiceException("没有权限访问角色数据！");
            //}
        }
    }

    /**
     * 通过角色ID查询角色使用数量
     * 
     * @param roleId 角色ID
     * @return 结果
     */
    public int countUserRoleByRoleId(Long roleId)
    {
        return sysUserRoleDao.countUserRoleByRoleId(roleId);
    }

    /**
     * 新增保存角色信息
     * 
     * @param role 角色信息
     * @return 结果
     */
    @Transactional
    public int insertRole(SysRole role)
    {
        // 新增角色信息
        sysRoleDao.addObject(role);
        return insertRoleMenu(role);
    }

    /**
     * 修改保存角色信息
     * 
     * @param role 角色信息
     * @return 结果
     */
    @Transactional
    public int updateRole(SysRole role)
    {
        // 修改角色信息
        sysRoleDao.updateObject(role);
        // 删除角色与菜单关联
        sysRoleMenuDao.deleteRoleMenuByRoleId(role.getRoleId());
        return insertRoleMenu(role);
    }

    /**
     * 修改角色状态
     * 
     * @param role 角色信息
     * @return 结果
     */
    public void updateRoleStatus(SysRole role)
    {
        sysRoleDao.updateObject(role);
    }

    /**
     * 修改数据权限信息
     * 
     * @param role 角色信息
     * @return 结果
     */
    @Transactional
    public void authDataScope(SysRole role)
    {
        // 修改角色信息
        sysRoleDao.updateObject(role);
        // 删除角色与部门关联
        sysRoleDeptDao.deleteRoleDeptByRoleId(role.getRoleId());
        // 新增角色和部门信息（数据权限）
        insertRoleDept(role);
    }

    /**
     * 新增角色菜单信息
     * 
     * @param role 角色对象
     */
    public int insertRoleMenu(SysRole role)
    {
        int rows = 1;
        // 新增用户与角色管理
        List<SysRoleMenu> list = new ArrayList<SysRoleMenu>();
        for (Long menuId : role.getMenuIds())
        {
            SysRoleMenu rm = new SysRoleMenu();
            rm.setRoleId(role.getRoleId());
            rm.setMenuId(menuId);
            list.add(rm);
        }
        if (list.size() > 0)
        {
            sysRoleMenuDao.saveList(list);
            //rows = roleMenuMapper.batchRoleMenu(list);
        }
        return rows;
    }

    /**
     * 新增角色部门信息(数据权限)
     *
     * @param role 角色对象
     */
    public void insertRoleDept(SysRole role)
    {
        int rows = 1;
        // 新增角色与部门（数据权限）管理
        List<SysRoleDept> list = new ArrayList<SysRoleDept>();
        for (Long deptId : role.getDeptIds())
        {
            SysRoleDept rd = new SysRoleDept();
            rd.setRoleId(role.getRoleId());
            rd.setDeptId(deptId);
            list.add(rd);
        }
        if (list.size() > 0)
        {
            sysRoleDeptDao.saveList(list);
        }
    }

    /**
     * 通过角色ID删除角色
     * 
     * @param roleId 角色ID
     * @return 结果
     */
    @Transactional
    public void deleteRoleById(Long roleId)
    {
        // 删除角色与菜单关联
        sysRoleMenuDao.deleteRoleMenuByRoleId(roleId);
        // 删除角色与部门关联
        sysRoleDeptDao.deleteRoleDeptByRoleId(roleId);
        sysRoleDao.deleteObject(roleId);
    }

    /**
     * 批量删除角色信息
     * 
     * @param roleIds 需要删除的角色ID
     * @return 结果
     */
    @Transactional
    public void deleteRoleByIds(Long[] roleIds)
    {
        for (Long roleId : roleIds)
        {
            checkRoleAllowed(new SysRole(roleId));
            SysRole role = (SysRole) getObject(roleId);
            if (countUserRoleByRoleId(roleId) > 0)
            {
                throw new ServiceException(String.format("%1$s已分配,不能删除", role.getRoleName()));
            }
        }
        // 删除角色与菜单关联
        sysRoleMenuDao.deleteRoleMenu(roleIds);
        // 删除角色与部门关联
        sysRoleDeptDao.deleteRoleDept(roleIds);
        //删除角色
        this.deleteObject(roleIds);
    }

    /**
     * 取消授权用户角色
     * 
     * @param userRole 用户和角色关联信息
     * @return 结果
     */
    public int deleteAuthUser(SysUserRole userRole)
    {
        return sysUserRoleDao.deleteUserRoleInfo(userRole);
    }

    /**
     * 批量取消授权用户角色
     * 
     * @param roleId 角色ID
     * @param userIds 需要取消授权的用户数据ID
     * @return 结果
     */
    public int deleteAuthUsers(Long roleId, Long[] userIds)
    {
        return sysUserRoleDao.deleteUserRoleInfos(roleId, userIds);
    }

    /**
     * 批量选择授权用户角色
     * 
     * @param roleId 角色ID
     * @param userIds 需要删除的用户数据ID
     * @return 结果
     */
    @Transactional
    public void insertAuthUsers(Long roleId, Long[] userIds)
    {
        // 新增用户与角色管理
        List<SysUserRole> list = new ArrayList<SysUserRole>();
        for (Long userId : userIds)
        {
            SysUserRole ur = new SysUserRole();
            ur.setUserId(userId);
            ur.setRoleId(roleId);
            list.add(ur);
        }
        sysUserRoleDao.saveList(list);
    }

}
