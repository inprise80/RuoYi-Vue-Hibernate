package com.gt.system.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.gt.common.core.dao.BaseDao;
import com.gt.common.core.domain.entity.SysDept;
import com.gt.common.core.page.Page;
import com.gt.common.core.service.BaseService;
import com.gt.common.utils.ValidateUtil;
import com.gt.system.dao.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.gt.common.annotation.DataScope;
import com.gt.common.constant.UserConstants;
import com.gt.common.core.domain.entity.SysRole;
import com.gt.common.core.domain.entity.SysUser;
import com.gt.common.exception.ServiceException;
import com.gt.common.utils.SecurityUtils;
import com.gt.common.utils.StringUtils;
import com.gt.common.utils.spring.SpringUtils;
import com.gt.system.domain.SysPost;
import com.gt.system.domain.SysUserPost;
import com.gt.system.domain.SysUserRole;

import javax.annotation.Resource;

/**
 * 用户 业务层处理
 * 
 * @author liuyj
 */
@Service
@Transactional
public class SysUserService extends BaseService<SysUser,Long>
{
    private static final Logger log = LoggerFactory.getLogger(SysUserService.class);

    @Autowired
    private SysUserDao sysUserDao;

    @Autowired
    private SysRoleDao sysRoleDao;

    @Autowired
    private SysPostDao sysPostDao;

    @Autowired SysDeptDao sysDeptDao;

    @Resource
    private SysUserRoleDao sysUserRoleDao;

    @Resource
    private SysUserPostDao sysUserPostDao;

    @Autowired
    private SysConfigService sysConfigService;
    @Override
    protected BaseDao<SysUser, Long> getDao() {
        return sysUserDao;
    }

    /**
     * 根据条件分页查询用户列表
     * 
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    
    @DataScope(deptAlias = "d", userAlias = "u")
    public Page findUserList(SysUser user)
    {
        return sysUserDao.findUserList(user);
    }

    /**
     * 根据条件分页查询已分配用户角色列表
     * 
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    
    @DataScope(deptAlias = "d", userAlias = "u")
    public Page findAllocatedList(SysUser user)
    {
        return sysUserDao.findAllocatedList(user);
    }

    /**
     * 根据条件分页查询未分配用户角色列表
     * 
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    
    @DataScope(deptAlias = "d", userAlias = "u")
    public Page findUnallocatedList(SysUser user)
    {
        return sysUserDao.findUnallocatedList(user);
    }

    /**
     * 通过用户名查询用户
     * 
     * @param userName 用户名
     * @return 用户对象信息
     */
    
    public SysUser selectUserByUserName(String userName)
    {
        SysUser sysUser= sysUserDao.selectUserByUserName(userName);
        sysUser = formatUser(sysUser);
        return sysUser;
    }

    /**
     *
     * @param sysUser 用户
     * @return 增加部门对象、角色List的用户对象
     */
    private SysUser formatUser(SysUser sysUser){
        List<SysUserRole> userRoleList = sysUserRoleDao.findByUserId(sysUser.getUserId());
        if (!ValidateUtil.isEmpty(userRoleList)) {
            List<Long> roleIds = userRoleList.stream().map(r -> r.getRoleId()).collect(Collectors.toList());
            List<SysRole> roleList = sysRoleDao.findByRoleIdIn(roleIds.stream().toArray(Long[]::new));
            sysUser.setRoleIds(roleIds.stream().toArray(Long[]::new));
            sysUser.setRoles(roleList);
        }
        SysDept sysDept =(SysDept) sysDeptDao.getObject(sysUser.getDeptId());
        sysUser.setDept(sysDept);
        List<SysUserPost> userPostList = sysUserPostDao.findByUserId(sysUser.getUserId());
        if (!userPostList.isEmpty()) {
            Long[] postIds = userPostList.stream().map(p -> p.getPostId()).toArray(Long[]::new);
            sysUser.setPostIds(postIds);
        }
        return sysUser;
    }

    /**
     * 查询用户所属角色组
     * 
     * @param userName 用户名
     * @return 结果
     */
    
    public String selectUserRoleGroup(String userName)
    {
        List<SysRole> list = sysRoleDao.findRolesByUserName(userName);
        StringBuffer idsStr = new StringBuffer();
        for (SysRole role : list)
        {
            idsStr.append(role.getRoleName()).append(",");
        }
        if (StringUtils.isNotEmpty(idsStr.toString()))
        {
            return idsStr.substring(0, idsStr.length() - 1);
        }
        return idsStr.toString();
    }

    /**
     * 查询用户所属岗位组
     * 
     * @param userName 用户名
     * @return 结果
     */
    
    public String selectUserPostGroup(String userName)
    {
        List<SysPost> list = sysPostDao.findPostsByUserName(userName);
        StringBuffer idsStr = new StringBuffer();
        for (SysPost post : list)
        {
            idsStr.append(post.getPostName()).append(",");
        }
        if (StringUtils.isNotEmpty(idsStr.toString()))
        {
            return idsStr.substring(0, idsStr.length() - 1);
        }
        return idsStr.toString();
    }

    /**
     * 校验用户名称是否唯一
     * 
     * @param userName 用户名称
     * @return 结果
     */
    
    public String checkUserNameUnique(String userName)
    {
        int count = sysUserDao.checkUserNameUnique(userName);
        if (count > 0)
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验用户名称是否唯一
     *
     * @param user 用户信息
     * @return
     */
    
    public String checkPhoneUnique(SysUser user)
    {
        Long userId = StringUtils.isNull(user.getUserId()) ? -1L : user.getUserId();
        SysUser info = sysUserDao.checkPhoneUnique(user.getPhonenumber());
        if (StringUtils.isNotNull(info) && info.getUserId().longValue() != userId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验email是否唯一
     *
     * @param user 用户信息
     * @return
     */
    
    public String checkEmailUnique(SysUser user)
    {
        Long userId = StringUtils.isNull(user.getUserId()) ? -1L : user.getUserId();
        SysUser info = sysUserDao.checkEmailUnique(user.getEmail());
        if (StringUtils.isNotNull(info) && info.getUserId().longValue() != userId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验用户是否允许操作
     * 
     * @param user 用户信息
     */
    
    public void checkUserAllowed(SysUser user)
    {
        if (StringUtils.isNotNull(user.getUserId()) && user.isAdmin())
        {
            throw new ServiceException("不允许操作超级管理员用户");
        }
    }

    /**
     * 校验用户是否有数据权限
     * 
     * @param userId 用户id
     */
    
    public void checkUserDataScope(Long userId)
    {
        if (!SysUser.isAdmin(SecurityUtils.getUserId()))
        {
            SysUser user = new SysUser();
            user.setUserId(userId);
            Page page = SpringUtils.getAopProxy(this).findUserList(user);
            if (StringUtils.isEmpty(page.getItems()))
            {
                throw new ServiceException("没有权限访问用户数据！");
            }
        }
    }

    /**
     * 新增保存用户信息
     * 
     * @param user 用户信息
     * @return 结果
     */
    
    @Transactional
    public void insertUser(SysUser user)
    {
        // 新增用户信息
        sysUserDao.saveObject(user);
        // 新增用户岗位关联
        insertUserPost(user);
        // 新增用户与角色管理
        insertUserRole(user);
    }

    /**
     * 注册用户信息
     * 
     * @param user 用户信息
     * @return 结果
     */
    
    public boolean registerUser(SysUser user)
    {
        sysUserDao.addObject(user);
        return true;
    }

    /**
     * 修改保存用户信息
     * 
     * @param user 用户信息
     * @return 结果
     */
    
    @Transactional
    public void updateUser(SysUser user)
    {
        Long userId = user.getUserId();
        // 删除用户与角色关联
        sysUserRoleDao.deleteUserRoleByUserId(userId);
        // 新增用户与角色管理
        insertUserRole(user);
        // 删除用户与岗位关联
        sysUserPostDao.deleteUserPostByUserId(userId);
        // 新增用户与岗位管理
        insertUserPost(user);
        sysUserDao.updateObject(user);
    }

    /**
     * 用户授权角色
     * 
     * @param userId 用户ID
     * @param roleIds 角色组
     */
    
    @Transactional
    public void insertUserAuth(Long userId, Long[] roleIds)
    {
        sysUserRoleDao.deleteUserRoleByUserId(userId);
        insertUserRole(userId, roleIds);
    }

    /**
     * 修改用户状态
     * 
     * @param user 用户信息
     * @return 结果
     */
    
    public void updateUserStatus(SysUser user)
    {
        sysUserDao.updateObject(user);
    }

    /**
     * 修改用户基本信息
     * 
     * @param user 用户信息
     * @return 结果
     */
    @Transactional
    public void updateUserProfile(SysUser user)
    {
        sysUserDao.updateObject(user);
    }

    /**
     * 修改用户头像
     * 
     * @param userName 用户名
     * @param avatar 头像地址
     * @return 结果
     */
    
    public boolean updateUserAvatar(String userName, String avatar)
    {
        return sysUserDao.updateUserAvatar(userName, avatar) > 0;
    }

    /**
     * 重置用户密码
     * 
     * @param userName 用户名
     * @param password 密码
     * @return 结果
     */
    
    public int resetUserPwd(String userName, String password)
    {
        return sysUserDao.resetUserPwd(userName, password);
    }

    /**
     * 新增用户角色信息
     * 
     * @param user 用户对象
     */
    public void insertUserRole(SysUser user)
    {
        Long[] roles = user.getRoleIds();
        if (StringUtils.isNotNull(roles))
        {
            // 新增用户与角色管理
            List<SysUserRole> list = new ArrayList<SysUserRole>();
            for (Long roleId : roles)
            {
                SysUserRole ur = new SysUserRole();
                ur.setUserId(user.getUserId());
                ur.setRoleId(roleId);
                list.add(ur);
            }
            if (list.size() > 0)
            {
                sysUserRoleDao.saveList(list);
            }
        }
    }

    /**
     * 新增用户岗位信息
     * 
     * @param user 用户对象
     */
    public void insertUserPost(SysUser user)
    {
        Long[] posts = user.getPostIds();
        if (StringUtils.isNotNull(posts))
        {
            // 新增用户与岗位管理
            List<SysUserPost> list = new ArrayList<SysUserPost>();
            for (Long postId : posts)
            {
                SysUserPost up = new SysUserPost();
                up.setUserId(user.getUserId());
                up.setPostId(postId);
                list.add(up);
            }
            if (list.size() > 0)
            {
                sysUserPostDao.saveList(list);
            }
        }
    }

    /**
     * 新增用户角色信息
     * 
     * @param userId 用户ID
     * @param roleIds 角色组
     */
    public void insertUserRole(Long userId, Long[] roleIds)
    {
        if (StringUtils.isNotNull(roleIds))
        {
            // 新增用户与角色管理
            List<SysUserRole> list = new ArrayList<SysUserRole>();
            for (Long roleId : roleIds)
            {
                SysUserRole ur = new SysUserRole();
                ur.setUserId(userId);
                ur.setRoleId(roleId);
                list.add(ur);
            }
            if (list.size() > 0)
            {
                sysUserRoleDao.saveList(list);
            }
        }
    }

    /**
     * 通过用户ID删除用户
     * 
     * @param userId 用户ID
     * @return 结果
     */
    
    @Transactional
    public void deleteUserById(Long userId)
    {
        // 删除用户与角色关联
        sysUserRoleDao.deleteUserRoleByUserId(userId);
        // 删除用户与岗位表
        sysUserPostDao.deleteUserPostByUserId(userId);
        sysUserDao.deleteObject(userId);
    }

    /**
     * 批量删除用户信息
     * 
     * @param userIds 需要删除的用户ID
     * @return 结果
     */
    
    @Transactional
    public void deleteUserByIds(Long[] userIds)
    {
        for (Long userId : userIds)
        {
            checkUserAllowed(new SysUser(userId));
        }
        // 删除用户与角色关联
        sysUserRoleDao.deleteUserRole(userIds);
        // 删除用户与岗位关联
        sysUserPostDao.deleteUserPost(userIds);
        this.deleteObject(userIds);
    }

    /**
     * 导入用户数据
     * 
     * @param userList 用户数据列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    
    public String importUser(List<SysUser> userList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(userList) || userList.size() == 0)
        {
            throw new ServiceException("导入用户数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        String password = sysConfigService.selectConfigByKey("sys.user.initPassword");
        for (SysUser user : userList)
        {
            try
            {
                // 验证是否存在这个用户
                SysUser u = sysUserDao.selectUserByUserName(user.getUserName());
                if (StringUtils.isNull(u))
                {
                    user.setPassword(SecurityUtils.encryptPassword(password));
                    user.setCreateBy(operName);
                    this.insertUser(user);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、账号 " + user.getUserName() + " 导入成功");
                }
                else if (isUpdateSupport)
                {
                    user.setUpdateBy(operName);
                    this.updateUser(user);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、账号 " + user.getUserName() + " 更新成功");
                }
                else
                {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、账号 " + user.getUserName() + " 已存在");
                }
            }
            catch (Exception e)
            {
                failureNum++;
                String msg = "<br/>" + failureNum + "、账号 " + user.getUserName() + " 导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0)
        {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        }
        else
        {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }
}
