package com.gt.system.service;

import com.gt.common.constant.UserConstants;
import com.gt.common.core.dao.BaseDao;
import com.gt.common.core.domain.entity.SysDictData;
import com.gt.common.core.domain.entity.SysDictType;
import com.gt.common.core.page.Page;
import com.gt.common.core.service.BaseService;
import com.gt.common.exception.ServiceException;
import com.gt.common.utils.DictUtils;
import com.gt.common.utils.StringUtils;
import com.gt.system.dao.SysDictDataDao;
import com.gt.system.dao.SysDictTypeDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.PostConstruct;
import java.util.List;

/**
 * 字典 业务层处理
 * 
 * @author liuyj
 */
@Service
public class SysDictTypeService extends BaseService<SysDictType,Long>
{
    @Autowired
    private SysDictTypeDao sysDictTypeDao;

    @Autowired
    private SysDictDataDao sysDictDataDao;
    @Override
    protected BaseDao<SysDictType, Long> getDao() {
        return sysDictTypeDao;
    }

    /**
     * 项目启动时，初始化字典到缓存
     */
    @PostConstruct
    public void init()
    {
        loadingDictCache();
    }

    /**
     * 根据条件分页查询字典类型
     * 
     * @param dictType 字典类型信息
     * @return 字典类型集合信息
     */
    
    public Page findDictTypeList(SysDictType dictType)
    {
        return sysDictTypeDao.findDictTypeList(dictType);
    }
  

    /**
     * 根据字典类型查询字典数据
     * 
     * @param dictType 字典类型
     * @return 字典数据集合信息
     */
    
    public List<SysDictData> findDictDataByType(String dictType)
    {
        List<SysDictData> dictDatas = DictUtils.getDictCache(dictType);
        if (StringUtils.isNotEmpty(dictDatas))
        {
            return dictDatas;
        }
        dictDatas = sysDictDataDao.findDictDataByType(dictType);
        if (StringUtils.isNotEmpty(dictDatas))
        {
            DictUtils.setDictCache(dictType, dictDatas);
            return dictDatas;
        }
        return null;
    }

    /**
     * 根据字典类型查询信息
     * 
     * @param dictType 字典类型
     * @return 字典类型
     */
    
    public SysDictType selectDictTypeByType(String dictType)
    {
        return sysDictTypeDao.selectDictTypeByType(dictType);
    }

    /**
     * 批量删除字典类型信息
     * 
     * @param dictIds 需要删除的字典ID
     * @return 结果
     */
    
    public void deleteDictTypeByIds(Long[] dictIds)
    {
        for (Long dictId : dictIds)
        {
            SysDictType dictType = (SysDictType) getObject(dictId);
            if (sysDictDataDao.countDictDataByType(dictType.getDictType()) > 0)
            {
                throw new ServiceException(String.format("%1$s已分配,不能删除", dictType.getDictName()));
            }
            sysDictTypeDao.deleteObject(dictId);
            DictUtils.removeDictCache(dictType.getDictType());
        }
    }

    /**
     * 加载字典缓存数据
     */
    
    public void loadingDictCache()
    {
        List<SysDictType> dictTypeList = sysDictTypeDao.getObjects();
        for (SysDictType dictType : dictTypeList)
        {
            List<SysDictData> dictDatas = sysDictDataDao.findDictDataByType(dictType.getDictType());
            DictUtils.setDictCache(dictType.getDictType(), dictDatas);
        }
    }

    /**
     * 清空字典缓存数据
     */
    
    public void clearDictCache()
    {
        DictUtils.clearDictCache();
    }

    /**
     * 重置字典缓存数据
     */
    
    public void resetDictCache()
    {
        clearDictCache();
        loadingDictCache();
    }

    /**
     * 新增保存字典类型信息
     * 
     * @param dict 字典类型信息
     * @return 结果
     */
    
    public void insertDictType(SysDictType dict)
    {
        sysDictTypeDao.addObject(dict);
        DictUtils.setDictCache(dict.getDictType(), null);
    }

    /**
     * 修改保存字典类型信息
     * 
     * @param dict 字典类型信息
     * @return 结果
     */
    
    @Transactional
    public void updateDictType(SysDictType dict)
    {
        SysDictType oldDict = (SysDictType) sysDictTypeDao.getObject(dict.getDictId());
        sysDictDataDao.updateDictDataType(oldDict.getDictType(), dict.getDictType());
        sysDictTypeDao.updateObject(dict);
        List<SysDictData> dictDatas = sysDictDataDao.findDictDataByType(dict.getDictType());
        DictUtils.setDictCache(dict.getDictType(), dictDatas);
    }

    /**
     * 校验字典类型称是否唯一
     * 
     * @param dict 字典类型
     * @return 结果
     */
    
    public String checkDictTypeUnique(SysDictType dict)
    {
        Long dictId = StringUtils.isNull(dict.getDictId()) ? -1L : dict.getDictId();
        SysDictType dictType = sysDictTypeDao.selectDictTypeByType(dict.getDictType());
        if (StringUtils.isNotNull(dictType) && dictType.getDictId().longValue() != dictId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }
}
