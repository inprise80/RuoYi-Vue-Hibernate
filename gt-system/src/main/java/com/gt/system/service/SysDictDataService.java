package com.gt.system.service;

import java.util.List;

import com.gt.common.core.dao.BaseDao;
import com.gt.common.core.page.Page;
import com.gt.common.core.service.BaseService;
import com.gt.system.dao.SysDictDataDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.gt.common.core.domain.entity.SysDictData;
import com.gt.common.utils.DictUtils;

/**
 * 字典 业务层处理
 * 
 * @author liuyj
 */
@Service
public class SysDictDataService extends BaseService<SysDictData,Long>
{
    @Autowired
    private SysDictDataDao sysDictDataDao;
    @Override
    protected BaseDao<SysDictData, Long> getDao() {
        return sysDictDataDao;
    }

    /**
     * 根据条件分页查询字典数据
     * 
     * @param dictData 字典数据信息
     * @return 字典数据集合信息
     */    
    public Page findDictDataList(SysDictData dictData)
    {
        return sysDictDataDao.findDictDataList(dictData);
    }

    /**
     * 根据字典类型和字典键值查询字典数据信息
     * 
     * @param dictType 字典类型
     * @param dictValue 字典键值
     * @return 字典标签
     */
    public String selectDictLabel(String dictType, String dictValue)
    {
        return sysDictDataDao.selectDictLabel(dictType, dictValue);
    }

    /**
     * 批量删除字典数据信息
     * 
     * @param dictCodes 需要删除的字典数据ID
     * @return 结果
     */
    public void deleteDictDataByIds(Long[] dictCodes)
    {
        for (Long dictCode : dictCodes)
        {
            SysDictData data = (SysDictData) this.getObject(dictCode);
            sysDictDataDao.deleteObject(dictCode);
            List<SysDictData> dictDatas = sysDictDataDao.findDictDataByType(data.getDictType());
            DictUtils.setDictCache(data.getDictType(), dictDatas);
        }
    }

    /**
     * 新增保存字典数据信息
     * 
     * @param data 字典数据信息
     * @return 结果
     */
    public void insertDictData(SysDictData data)
    {
        sysDictDataDao.addObject(data);
        List<SysDictData> dictDatas = sysDictDataDao.findDictDataByType(data.getDictType());
        DictUtils.setDictCache(data.getDictType(), dictDatas);
    }

    /**
     * 修改保存字典数据信息
     * 
     * @param data 字典数据信息
     * @return 结果
     */
    public void updateDictData(SysDictData data)
    {
        sysDictDataDao.updateObject(data);
        List<SysDictData> dictDatas = sysDictDataDao.findDictDataByType(data.getDictType());
        DictUtils.setDictCache(data.getDictType(), dictDatas);
    }


}
