package com.gt.system.service;

import com.gt.common.core.dao.BaseDao;
import com.gt.common.core.page.Page;
import com.gt.common.core.service.BaseService;
import com.gt.system.dao.SysOperLogDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.gt.system.domain.SysOperLog;
import org.springframework.transaction.annotation.Transactional;

/**
 * 操作日志 服务层处理
 * 
 * @author liuyj
 */
@Service
@Transactional
public class SysOperLogService extends BaseService<SysOperLog,Long>
{
    @Autowired
    private SysOperLogDao sysOperLogDao;
    @Override
    protected BaseDao<SysOperLog, Long> getDao() {
        return sysOperLogDao;
    }

    /**
     * 查询系统操作日志集合
     * 
     * @param operLog 操作日志对象
     * @return 操作日志集合
     */
    public Page findOperLogList(SysOperLog operLog)
    {
        return sysOperLogDao.findOperLogList(operLog);
    }

    /**
     * 保存日志
     * @param operLog 操作日志对象
     */
    public void savaData(SysOperLog operLog){
        sysOperLogDao.saveObject(operLog);
    }

    /**
     * 清空操作日志
     */
    public void cleanOperLog()
    {
        sysOperLogDao.cleanOperLog();
    }

}
