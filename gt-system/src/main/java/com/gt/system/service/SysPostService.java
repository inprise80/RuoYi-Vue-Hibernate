package com.gt.system.service;

import java.util.List;

import com.gt.common.core.dao.BaseDao;
import com.gt.common.core.page.Page;
import com.gt.common.core.service.BaseService;
import com.gt.system.dao.SysPostDao;
import com.gt.system.dao.SysUserPostDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.gt.common.constant.UserConstants;
import com.gt.common.exception.ServiceException;
import com.gt.common.utils.StringUtils;
import com.gt.system.domain.SysPost;
import javax.annotation.Resource;

/**
 * 岗位信息 服务层处理
 * 
 * @author liuyj
 */
@Service
public class SysPostService extends BaseService<SysPost,Long>
{
    @Autowired
    private SysPostDao sysPostDao;

    @Resource
    private SysUserPostDao sysUserPostDao;
    @Override
    protected BaseDao<SysPost, Long> getDao() {
        return sysPostDao;
    }

    /**
     * 查询岗位信息集合(分页)
     * 
     * @param post 岗位信息
     * @return 岗位信息集合
     */
    public Page findPostList(SysPost post)
    {
        return sysPostDao.findPostList(post);
    }


    /**
     * 根据用户ID获取岗位选择框列表
     * 
     * @param userId 用户ID
     * @return 选中岗位ID列表
     */
    public List<Long> findPostListByUserId(Long userId)
    {
        return sysPostDao.findPostListByUserId(userId);
    }

    /**
     * 校验岗位名称是否唯一
     * 
     * @param post 岗位信息
     * @return 结果
     */
    public String checkPostNameUnique(SysPost post)
    {
        Long postId = StringUtils.isNull(post.getPostId()) ? -1L : post.getPostId();
        SysPost info = sysPostDao.checkPostNameUnique(post.getPostName());
        if (StringUtils.isNotNull(info) && info.getPostId().longValue() != postId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验岗位编码是否唯一
     * 
     * @param post 岗位信息
     * @return 结果
     */
    public String checkPostCodeUnique(SysPost post)
    {
        Long postId = StringUtils.isNull(post.getPostId()) ? -1L : post.getPostId();
        SysPost info = sysPostDao.checkPostCodeUnique(post.getPostCode());
        if (StringUtils.isNotNull(info) && info.getPostId().longValue() != postId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 通过岗位ID查询岗位使用数量
     * 
     * @param postId 岗位ID
     * @return 结果
     */
    public int countUserPostById(Long postId)
    {
        return sysUserPostDao.countUserPostById(postId);
    }


    /**
     * 批量删除岗位信息
     * 
     * @param postIds 需要删除的岗位ID
     * @return 结果
     * @throws Exception 异常
     */
    public void deletePostByIds(Long[] postIds)
    {
        for (Long postId : postIds)
        {
            SysPost post = (SysPost) getObject(postId);
            if (countUserPostById(postId) > 0)
            {
                throw new ServiceException(String.format("%1$s已分配,不能删除", post.getPostName()));
            }
        }
        this.deleteObject(postIds);
    }


}
