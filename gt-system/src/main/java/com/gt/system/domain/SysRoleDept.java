package com.gt.system.domain;

import com.gt.system.domain.groupkey.SysRoleDeptKey;
import javax.persistence.*;
import java.io.Serializable;


/**
 * 角色和部门关联 sys_role_dept
 *
 * @author liuyj
 */
@Entity
@Table(name = "sys_role_dept")
@IdClass(SysRoleDeptKey.class)
public class SysRoleDept implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 角色ID
     */
    @Id
    @Column(name = "role_id")
    private Long roleId;

    /**
     * 部门ID
     */
    @Id
    @Column(name = "dept_id")
    private Long deptId;

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }
}