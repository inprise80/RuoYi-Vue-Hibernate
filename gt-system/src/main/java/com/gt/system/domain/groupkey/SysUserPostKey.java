package com.gt.system.domain.groupkey;
import java.io.Serializable;
/**
 * 用户和岗位关联 sys_user_post 联合主键
 *
 * @author liuyj
 */

public class SysUserPostKey implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 岗位ID
     */
    private Long postId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getPostId() {
        return postId;
    }

    public void setPostId(Long postId) {
        this.postId = postId;
    }
}