package com.gt.system.domain;

import com.gt.system.domain.groupkey.SysUserRoleKey;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 用户和角色关联 sys_user_role
 *
 * @author liuyj
 */
@Entity
@Table(name = "sys_user_role")
@IdClass(SysUserRoleKey.class)
public class SysUserRole implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户ID
     */
    @Id
    @Column(name = "user_id")
    private Long userId;

    /**
     * 角色ID
     */
    @Id
    @Column(name = "role_id")
    private Long roleId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }
}