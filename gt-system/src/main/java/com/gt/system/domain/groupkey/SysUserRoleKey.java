package com.gt.system.domain.groupkey;
import java.io.Serializable;

/**
 * 用户和角色关联 sys_user_role 联合主键
 *
 * @author liuyj
 */
public class SysUserRoleKey implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 角色ID
     */
    private Long roleId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }
}