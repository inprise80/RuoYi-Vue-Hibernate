package com.gt.system.domain;

import com.gt.system.domain.groupkey.SysUserPostKey;
import javax.persistence.*;
import java.io.Serializable;


/**
 * 用户和岗位关联 sys_user_post
 *
 * @author liuyj
 */
@Entity
@Table(name = "sys_user_post")
@IdClass(SysUserPostKey.class)
public class SysUserPost implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 用户ID
     */
    @Id
    @Column(name = "user_id")
    private Long userId;

    /**
     * 岗位ID
     */
    @Id
    @Column(name = "post_id")
    private Long postId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getPostId() {
        return postId;
    }

    public void setPostId(Long postId) {
        this.postId = postId;
    }
}