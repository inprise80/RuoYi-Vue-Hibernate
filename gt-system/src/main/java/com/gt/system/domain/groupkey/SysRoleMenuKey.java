package com.gt.system.domain.groupkey;
import java.io.Serializable;

/**
 * 角色和菜单关联 sys_role_menu 联合主键
 *
 * @author liuyj
 */

public class SysRoleMenuKey implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 角色ID
     */
    private Long roleId;

    /**
     * 菜单ID
     */
    private Long menuId;

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Long getMenuId() {
        return menuId;
    }

    public void setMenuId(Long menuId) {
        this.menuId = menuId;
    }
}