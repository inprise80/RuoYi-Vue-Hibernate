package com.gt.system.domain;

import com.gt.system.domain.groupkey.SysRoleMenuKey;
import javax.persistence.*;
import java.io.Serializable;


/**
 * 角色和菜单关联 sys_role_menu
 *
 * @author liuyj
 */
@Entity
@Table(name = "sys_role_menu")
@IdClass(SysRoleMenuKey.class)
public class SysRoleMenu implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 角色ID
     */
    @Id
    @Column(name = "role_id")
    private Long roleId;

    /**
     * 菜单ID
     */
    @Id
    @Column(name = "menu_id")
    private Long menuId;

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Long getMenuId() {
        return menuId;
    }

    public void setMenuId(Long menuId) {
        this.menuId = menuId;
    }
}